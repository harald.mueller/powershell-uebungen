﻿$data = Get-Content .\rechnung21003.data

$absender_nam = "";
$absender_adr = "";
$absender_ort = "";
$absender_mws = "";

$endkunde_nam = "";
$endkunde_adr = "";
$endkunde_ort = "";

[array]$rPos;

foreach ($line in $data) {
    if ($line.StartsWith("Rechnung")) {
        $rechnung = $line.Split(";")
        $r = $rechnung[3];
        $r = $rechnung[4];
        $r = $rechnung[5];
    }
    if ($line.StartsWith("Herkunft")) {
        $herkunft = $line.Split(";")
        $absender_nam = $herkunft[3];
        $absender_adr = $herkunft[4];
        $absender_ort = $herkunft[5];
        $absender_mws = $herkunft[6];
    }
    if ($line.StartsWith("Endkunde")) {
        $endkunde = $line.Split(";")
        $endkunde_nam = $endkunde[2];
        $endkunde_adr = $endkunde[3];
        $endkunde_ort = $endkunde[4];
    }
    if ($line.StartsWith("RechnPos")) {
        $rechnPos = $line.Split(";")
        $posNr = $rechnPos[1];
        [string[]]$rP = 
        $rPos[$posNr]["Text"] = $rechnPos[2];
        $rPos[$posNr]["pStk"] = $rechnPos[3];
        $rPos[$posNr]["Einh"] = $rechnPos[4];
        $rPos[$posNr]["PTot"] = $rechnPos[5];
    }
}

echo ""
echo "Ausgabe Absender:"
echo $absender_nam
echo $absender_adr
echo $absender_ort

echo ""
echo "Ausgabe Endkunde"
echo $endkunde_nam
echo $endkunde_adr
echo $endkunde_ort