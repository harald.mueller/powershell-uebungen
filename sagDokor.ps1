﻿$frage = ""
cls

function sayDoctor([string]$eingabe) {
    $ret = "" 
    $antwort = $eingabe.Replace("Ich habe", "Sie haben")
    $antwort = $eingabe.Replace("Ich bin", "Sie sind")
    $antwort = $eingabe.Replace("Ich fühle", "Sie fühlen")
    $antwort = $eingabe.Replace("Ich spüre", "Sie spüren")
    $antwort = $eingabe.Replace("Ich meine", "Sie meinen")

    if ($eingabe.StartsWith("es ")) {
        $antwort = $eingabe.Replace("es ", " Wenn es Ihnen ")
    }

    if ($eingabe.Contains(" mir ")) {
        $antwort = $eingabe.Replace("mir ", "")
        $antwort += " Ihnen"
    }

    if ($eingabe.Contains(" geht ")) {
        $antwort = $eingabe.Replace("geht ", "")
        $antwort += " geht"
    }


    $rand = Get-Random -Minimum 0 -Maximum 5
    Start-Sleep -Seconds $rand
    Switch ($rand) { 
        0 { $ret = " Wenn Sie sagen '$eingabe'.";$ret += getVarianz("Gegenfrage") } 
        1 { $ret = getVarianz("Aufforderung") } 
        2 { $ret = getVarianz("Gefuehl") } 
        3 { $ret = getVarianz("Gefuehl"); $ret += getVarianz("Aufforderung") }
        4 { $ret = getVarianz("Verstaendnis"); $ret += $antwort; $ret += getVarianz("Gefuehl") } 
        5 { $ret = getVarianz("Verstaendnis"); $ret += $antwort; $ret += getVarianz("Aufforderung") } 
    }
    if ($eingabe.Contains("nicht") -or
        $eingabe.Contains("schlecht") -or
        $eingabe.Contains("traurig")      
        ) {
            $m = getVarianz("Mitleid")
            $ret = "$m $ret" 
    } 
    return $ret
}

function getVarianz([string]$var) {
    $a = ""
    $r = Get-Random
    Switch ($var) {
        "Gegenfrage" {
            Switch ($r % 3) {
                0 {$a =". Wie meinen Sie das genau? "}
                1 {$a =". Erzählen Sie mehr? "}                
                2 {$a =". Wie gehen Sie damit um? "}
            }
        }
        "Gefuehl" {
            Switch ($r % 3) {
                0 {$a =". Wie haben Sie das herausgefunden? "}
                1 {$a =". Was macht das mit Ihnen? "}                
                2 {$a =". Wie geht es Ihnen dabei? "}
            }
        }
        "Mitleid" {
            Switch ($r % 5) {
                0 {$a =" Shit. "}
                1 {$a =" Das ist aber tragisch. "}                
                2 {$a =" Blöd. "}
                3 {$a =" Das geht vielen so. "}
                4 {$a =" Das kenn ich. "}                
                5 {$a =" Dumm. "}
            }
        }
        "Aufforderung" {
            Switch ($r % 3) {
                0 {$a =" Erzählen Sie.. "}
                1 {$a =" Und wie weiter? "}                
                2 {$a =" Und zwar? "}
            }
        }
        "Verstaendnis" {
            Switch ($r % 3) {
                0 {$a =" Verstehe. "}
                1 {$a =" Aha. "}                
                2 {$a =" Hmm. "}
            }
        }
    }
    return $a    
}

Write-Host "Wie geht es Ihnen heute?"
while($TRUE) {
    $in = Read-Host " (-" 
    $out = sayDoctor($in)
    Write-Host " :-O $out" -ForegroundColor Yellow
}
