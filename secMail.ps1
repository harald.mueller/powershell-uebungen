# Secure Mailer
################
# Autor: Harald Mueller
# Version 1.0: Juli 2017 
# Funktion: Verwendet SMTP-Mail 

#Parameter einlesen und typisieren
param([String]$Serv, [String]$Port, [String]$User, [String]$mailTo, [String]$subject, [String]$body, [String]$attachment)

# Parameter versichern
[String]$SMTPHost = "$Serv"
[String]$SMTPPort = "$Port"
if ($Port.Trim() -eq "") {
    $SMTPPort = "22"
}
[String]$Username = "$User"
[String]$To = "$mailTo"
[String]$subjectFixtext
if ($subject.Trim() -eq "") {
    $subjectFixtext = "Betreff"
} else {
    $subjectFixtext = $subject
}


$pfad1 = Split-Path -Path $body -IsAbsolute
$pfad2 = Split-Path -Path $body 
# Body-Text aus File einlesen 
if ($body.Trim() -eq "") {
    if ($body.Contains("\")) {
        $body = (Get-Content -Path ".\mail.txt" -ReadCount 0) -join "`n"    
    }
}

$Att = New-Object Net.Mail.Attachment($attachment)
# $Password = "meinPasswort" | ConvertTo-SecureString -AsPlainText -Force    
$Password = "$Pass" | ConvertTo-SecureString -AsPlainText -Force    

# Wenns Probleme mit dem Passwort gibt
# (Get-Credential).password | ConvertFrom-SecureString > mailpasswort.txt
# $pw = Get-Content .\mailpasswort.txt | ConvertTo-SecureString
# $cred = New-Object System.Management.Automation.PSCredential "MailUser", $pw
# Send-MailMessage -Credential $cred -to "harald.mueller@bluewin.ch" -from "HMue TBZ <harald.mueller@tbz.ch>" -Subject "Test" -body "Test fuer Send-MailMessage"



# Das Mail zusammenbauen
$message = New-Object System.Net.Mail.MailMessage
$message.from = $Username
$message.body = $body
$message.To.Add($To)
$message.Attachments.Add($Att)
$message.Subject = $subjectFixtext 

$smtp = New-Object System.Net.Mail.SmtpClient($SMTPHost, $SMTPPort);
$smtp.EnableSSL = $true
##$smtp.Credentials = New-Object System.Net.NetworkCredential($Username, $Password);

$smtp.Send($message)
