﻿Write-host "Menu"
Write-host "----------------"
Write-host "1 Montag"
Write-host "2 Dienstag"
Write-host "3 Mittwoch"
Write-host "4 Donnerstag"
Write-host "7 Mein BM-Index berechnen"
$auswahl = Read-Host "Wählen Sie 1-7"
switch ($auswahl) {
    "1" {
        "Am Montag ist Schule"
    }
    "2" {
        "Am Dienstag Abend ist Schwimmtraining"
        }
    "3" {
        "Am Mittwoch habe ich frei"
    }
    "4" {
        "Am Donnerstag habe an der TBZ"
    }
    "7" {
        [float]$kg = Read-Host "Dein Gewicht in kg"
        [float]$lg = Read-Host "Dein Körpergrösse in m"

        [float]$bmi = $kg / ($lg * $lg)
        $bmi = [math]::Round($bmi, 1)
        Write-Host "Der BMI ist = $bmi"
    }
    default {
        "Ungültige Eingabe"
    }
}



