#Funktion InputBox
##################

# Script: InputBox.ps1 ["Eingabetext"] ["Fenstertitel"]
# Einfache Texteingabe mit Formular und Abbruchmöglichkeiten
# Rückgabewerte: Eingabetext bei [OK]
#                "Cancel" bei [Cancel] oder [X]
# Autor: KEL
# Version 1.0: 25.10.12
# Version 1.1: 20.03.15 $script:answer wegen Gültigkeitsbereich eingefügt

#.Net Bibliothek Forms laden
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 

# Antwort für Abbruch presetten 
$script:answer="Cancel"

[Object] $Form = New-Object System.Windows.Forms.Form 
if ($args.Count -gt 1) {
   $Form.Text = $args[1]
} else {  
   $Form.Text = "PS-InputBox"
}
$Form.Size = New-Object System.Drawing.Size(700,500) 
$Form.StartPosition = "CenterScreen"


#OK-Button
$btnOK = New-Object System.Windows.Forms.Button
$btnOK.Location = New-Object System.Drawing.Size(375,200)
$btnOK.Size = New-Object System.Drawing.Size(220,100)
$btnOK.Text = "OK"
$btnOK.Add_Click({$script:answer=$TextBox.Text;$Form.Close()}) 
$Form.Controls.Add($btnOK)

#Cancel-Button
$btnCancel = New-Object System.Windows.Forms.Button
$btnCancel.Location = New-Object System.Drawing.Size(100,200)
$btnCancel.Size = New-Object System.Drawing.Size(220,100)
$btnCancel.Text = "Abbrechen"
$btnCancel.Add_Click({$script:answer="Cancel";$Form.Close()})
$Form.Controls.Add($btnCancel)

#Anzeigetext
$Label = New-Object System.Windows.Forms.Label
$Label.Location = New-Object System.Drawing.Size(20,40) 
$Label.Size = New-Object System.Drawing.Size(600,50) 
if ($args.count -gt 0) {
    $Label.Text = $args[0]
} else {  
	$Label.Text = "Input:"
}
$Form.Controls.Add($Label) 

#Textbox
$TextBox = New-Object System.Windows.Forms.TextBox 
$TextBox.Location = New-Object System.Drawing.Size(100,100) 
$TextBox.Size = New-Object System.Drawing.Size(500,100) 
$TextBox.BackColor = "LightYellow"
$TextBox.Font= "Arial, 24"
$Form.Controls.Add($TextBox) 

#Tastenaktionen einfügen
$Form.KeyPreview = $True
$Form.Add_KeyDown({if ($_.KeyCode -eq "Enter") 
    {$script:answer=$TextBox.Text;$Form.Close()}}) #Enter ist wie [OK]
$Form.Add_KeyDown({if ($_.KeyCode -eq "Escape") 
    {$script:answer="Cancel";$Form.Close()}}) #ESC ist wie [X]
	
#Initialisirung und starten des Formulars
$Form.Topmost = $True
$Form.Add_Shown({$Form.Activate()})
#Falls Abbruch mit [X]...
$Form.ShowDialog() | out-null

#Rückgabe des Eingabewertes
return $script:answer
