﻿<# 
            .SYNOPSIS  
            Shows a graphical message box, with various prompt types available. 
 
            .DESCRIPTION 
            Emulates the Visual Basic MsgBox function.  It takes four parameters, of which only the prompt is mandatory 
 
            .INPUTS 
            The parameters are:- 
             
            Prompt (mandatory):  
                Text string that you wish to display 
                 
            Title (optional): 
                The title that appears on the message box 
                 
            Icon (optional).  Available options are: 
                Information, Question, Critical, Exclamation (not case sensitive) 
                
            Buttons (optional). Available options are: 
                OKOnly, OkCancel, AbortRetryIgnore, YesNoCancel, YesNo, RetryCancel (not case sensitive)   
 
            .OUTPUTS 
            OK, Cancel, Yes or No 
 
            .EXAMPLE 
            C:\PS> Show-MsgBox Hello 
            Shows a popup message with the text "Hello", and the default box, icon and defaultbutton settings. 
 
            .EXAMPLE 
            C:\PS> Show-MsgBox -Prompt "This is the prompt" -Title "This Is The Title" -Icon Critical -BoxType YesNo -DefaultButton 2 
            Shows a popup with the parameter as supplied. 
 
            .LINK 
            http://www.itpilgrims.com/2012/07/show-msgbox/
  #> 

	Function Show-MsgBox {
	[CmdletBinding()]
	Param(
		[Parameter(Position=0, Mandatory=$True)] [string]$strMessage,
		[Parameter(Position=1, Mandatory=$False)] [string]$strTitle=" ",
		[Parameter(Position=2, Mandatory=$False)] [ValidateSet("None","Hand","Error","Stop","Question","Exclamation","Warning","Asterisk","Information")] [string]$strIcon="None",
		[Parameter(Position=3, Mandatory=$False)] [ValidateSet("OK","OKCancel","AbortRetryIgnore","YesNoCancel","YesNo","RetryCancel")] [string]$strButtons="OK",
		[Parameter(Position=4, Mandatory=$False)] [ValidateSet("Button1","Button2","Button3")] [string]$strDefaultButton="Button1"
	)
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null 
	$QueryResult = [Windows.Forms.MessageBox]::Show($strMessage, $strTitle, $strButtons, $strIcon, $strDefaultButton, "DefaultDesktopOnly")
	Return $QueryResult
}