﻿cls
[int]$Jasmins_Alter = 21
$global:Jasmins_KinderAnzahl = 0   # global !!
$KindsName = "?"
$Jasmins_Geschenk = "nix"

function Spitalaufenthalt() {    
    #Innerhalb einer Funktion gelten spezielle Regeln
    #wie zum Beispiel die "Kapselung" von (lokalen) Variabeln
    Write-Host "Fkt  - - - - - - - - - im Spital / beg Fkt"
    $Jasmins_KinderAnzahl++      #hier wird keine Zuweisung zum globalen Kontext gemacht
    $global:KindsName = "Kevin"  #hier wird  die  Zuweisung zum globalen Kontext gemacht
    $private:Jasmins_Geschenk = "Rote Tasche"
    Write-Host "Fkt  Jasmins Kinder-Zahl = "$Jasmins_KinderAnzahl
    Write-Host "Fkt  KindsName           = "$KindsName              -ForegroundColor:green
    Write-Host "Fkt  Geschenk (privat)   = "$Jasmins_Geschenk       -ForegroundColor:yellow
    Write-Host "Fkt  - - - - - - - - - im Spital / end Fkt"
}

Write-Host "================================"
Write-Host "A Jasmins Kinder-Zahl = "$Jasmins_KinderAnzahl
Write-Host "A Jasmins_Alter       = "$Jasmins_Alter
Write-Host "A KindsName           = "$KindsName                     -ForegroundColor:green
Write-Host "A Geschenk            = "$Jasmins_Geschenk              -ForegroundColor:yellow

Spitalaufenthalt  # <=---- Das ist der Funktionsaufruf. 

Write-Host "B Jasmins Kinder-Zahl = "$Jasmins_KinderAnzahl
Write-Host "B Jasmins_Alter       = "$Jasmins_Alter
Write-Host "B KindsName           = "$KindsName                     -ForegroundColor:green
Write-Host "B Geschenk            = "$Jasmins_Geschenk              -ForegroundColor:yellow
Write-Host "================================"
Write-Host ""

[bool]$is_heute_JasminsGeburtstag = $TRUE
if ($is_heute_JasminsGeburtstag) {
    Write-Host "C    Wenn Jasmin Geburtstag hat,"
    $Jasmins_Alter++
    Write-Host "C    ist Jasmins_Alter jetzt  = "$Jasmins_Alter
    Write-Host "C    Geschenk (bleibt geheim) = "$Jasmins_Geschenk   -ForegroundColor:yellow
}
Write-Host ""
Write-Host "D Jasmins_Alter  = "$Jasmins_Alter
Write-Host "D KindsName      = "$KindsName                         -ForegroundColor:green
Write-Host "D Geschenk       = "$Jasmins_Geschenk                  -ForegroundColor:yellow
Write-Host ""
