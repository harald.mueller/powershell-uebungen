﻿$config_file = "ftpconfig.txt"
# Herausschreiben der Login-Daten für FTP 
# (So kann man einfach ein File schreiben)
"ftp.haraldmueller.ch"       > $config_file # Login Host
"schoolerinvoices"          >> $config_file # Login User
"Berufsschule8005!"         >> $config_file # Login Passwort
"in/testschueler"           >> $config_file # in-Dir
"out/testschueler"          >> $config_file # out-Dir
# (Dieses File schreibt man normalerweise nicht mit dem Skript
# sondern von Hand. Hier soll nur gezeigt werden, wie man solche
# Logindaten als externe Datei halten kenn und in der 
# Skript-Anwendung relativ einfach eingelesen werden kann)
# ==========================================================

# ==========================================================
# HIER BEGINNT DAS SCRIPT

# Erzeigen des Tagesdatums als String
$date = Get-Date -format "yyyyMMdd"
$outdata = "protocol_"+$date+".log"

# Definieren von Testfiles
$t1 = "testdaten1.data"
$t2 = "testdaten2.data"

# Füllen von Testfiles
"Testdaten Inhalt 1"     > $t1
"Testdaten Inhalt 2"     > $t2

# Wenn man ein solches Logi-Datenfile hat, dann kann man
# entweder mit $ftpCredentials < $config_file oder auch mit
#   $ftpCredentials = Get-Content $config_file
# die Daten einlesen. 
$ftpCredentials = Get-Content $config_file

# Die Variable enthält dann die Daten als "Array". 
# Also die erste Zeile ist ist auf dem [0]-ten Element 
# --> in  $ftpCredentials[0]  steht nun  "ftp.haraldmueller.ch"


# Definieren eines Batch-Command-Files 
# zur Übergabe an das FTP-Programm
$ftpcommands = "ftpcommands.txt"

# Schreibe Inhalt der Batch-Commands
"open "+$ftpCredentials[0]   > $ftpcommands # Login Host
$ftpCredentials[1]          >> $ftpcommands # Login User
$ftpCredentials[2]          >> $ftpcommands # Login Passwort
"cd "+$ftpCredentials[3]    >> $ftpcommands

"put $t1"                   >> $ftpcommands # Variante 1
"send $t2"                  >> $ftpcommands # Variante 2
"dir"                       >> $ftpcommands
"rename $t1 renamed1.data"  >> $ftpcommands
"rename $t2 renamed2.data"  >> $ftpcommands
"put $t1"                   >> $ftpcommands 
"put $t2"                   >> $ftpcommands 
"bye"                       >> $ftpcommands # Abmelden / Exit FTP

# Aufruf des FTP-Standardprogramm im CMD, was auch im PS zur Verfügung steht,
# wobei die Konsolenausgaben in ein out-File umgeleitet werden
ftp -s:$ftpcommands > $outdata  
"===== Ende Verarbeitung 1 ====`n" >> $outdata 


# NEUES SCRIPT FUERS LOESCHEN
"open "+$ftpCredentials[0]   > $ftpcommands # Login Host
$ftpCredentials[1]          >> $ftpcommands # Login User
$ftpCredentials[2]          >> $ftpcommands # Login Passwort
"cd "+$ftpCredentials[3]    >> $ftpcommands

"delete renamed1.data"      >> $ftpcommands
"delete $t1"                >> $ftpcommands
"dir"                       >> $ftpcommands
"bye"                       >> $ftpcommands # Abmelden / Exit FTP

ftp -s:$ftpcommands         >> $outdata  
"===== Ende Verarbeitung 2 ===== ">> $outdata 
