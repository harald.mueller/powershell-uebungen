﻿# Funktionsdefinitionen

function Get-FtpDir ($url,$credentials) {
    $request = [Net.WebRequest]::Create($url)
    $request.Method = [System.Net.WebRequestMethods+FTP]::ListDirectory
    if ($credentials) { $request.Credentials = $credentials }
    $response = $request.GetResponse()
    $reader = New-Object IO.StreamReader $response.GetResponseStream() 
    $reader.ReadToEnd()
    $reader.Close()
    $response.Close()
}

function Del-File ($url,$credentials) {
    $request2 = [Net.WebRequest]::Create($url)
    $request2.Method = [System.Net.WebRequestMethods+FTP]::DeleteFile
    if ($credentials) { 
        $request2.Credentials = $credentials 
    }
    $response2 = $request2.GetResponse()
}



# Skriptbeginn
## -->  dies aus am Besten aus einer Config-Datei lesen 
$ServerName = "ftp.haraldmueller.ch"
$Username = "schoolerinvoices"
$Password = "Berufsschule8005!"
$folder = 'out/testschueler/'
$target = "C:\Users\haral\OneDrive\99-Technik\PowerShell\FTP\"
$multipleFileMask = "*.data"


$webclient = New-Object System.Net.WebClient 
$webclient.Credentials = New-Object System.Net.NetworkCredential($Username,$Password) 

$credentials = New-Object System.Net.NetworkCredential($Username,$Password) 
$folderPath = "ftp://$ServerName/$folder"

# Funktionsaufruf (holt alle Files)
$Allfiles=Get-FTPDir -url $folderPath -credentials $credentials
$files = ($Allfiles -split "`r`n")

$counter = 0
foreach ($file in ($files | where {$_ -like $multipleFileMask})){
    $remote  = $folderPath + $file  
    $destination = $target + $file 
    $webclient.DownloadFile($remote, $destination)

    #   Beim/für Upload, entsprechende Aenderungen machen
    ### $webclient.UploadFile($remote, $LocalFilePath)

    #   Heruntergeladene Dateien sollten gelöscht werden
    ### Del-File ($remote,$credentials)


    # 'show time ..' (diese und folgende 4 Zeilen kann man auch löschen, sie zeigen nur an, was passiert)
    $counter++
    echo $counter
    echo "source     :   $remote" 
    echo "destination:   $destination"
}
