﻿# Schaltjahr
#############
# Autor: Kel
# Version 1.0: 12.11.12 
# Funktion: Schaltjahrberechnung
# Eingabe über Inputbox.ps1 mit Fehlerabfrage und Verarbeitungsloop
# Ausgabe über Show-MsgBox-Funktion, die vorab geladen wird
# Übung 1 für Struktogrammübungen

# Konstanten-Deklaration:

# Variablen-Deklaration und Initialisation:
[string] $eingabe = ""  	# für Inputbox
[single] $jahr = 0		    # speichert Jahreszahl
[bool]   $weiter = $true	# für Ablaufsteuerung
[String] $scriptpath = Split-Path $MyInvocation.MyCommand.Path
							# aktueller Scriptpfad korrekt initialisieren
# Wechsle zu lokalem Script-Verzeichnis
Set-Location $scriptpath							

#Externe Funktion Show-MsgBox einbinden
. .\Show-MsgBox.ps1

# Hauptschleife: ganze Aufgabe wiederholen
Do {
        # Eingabewiederholung bei Falscheingaben
		Do {
		    #Setzt Flag für neue Eingabe auf "weiter"
		    $weiter = $true
			
			#Eingabe Jahreszahl als Text
			$eingabe = .\Inputbox.ps1 "Geben Sie eine Jahreszahl ein:" "Schaltjahr"			
			if ($eingabe -eq "Cancel") {
			    write "Programm abgebrochen..."
				exit
			}
			
			#Umwandlung Text in Zahl (Double) ohne Abbruch bei Fehler (=0)
			$jahr = $eingabe -as [double] #Siehe Kapitel 3
			
			#Eingabeüberprüfung (Robustheit)
			if ($jahr -eq 0){ 
			   Show-MsgBox "Falsche Eingabe: Bitte 4-stellige Zahl eingeben" "ERROR!" "Exclamation"
			   $weiter = $false
			}
		} Until ($weiter)

		#Schaltjahr berechnen
		if ($jahr % 4 -eq 0) {
			if ($jahr % 100 -eq 0) {
				if ($jahr % 400 -eq 0) {
					#Ausgabe Resultat z.B. 2000
					Show-MsgBox "$Jahr ist EIN Schaltjahr!" "Schaltjahr"	
				} else {
					#Ausgabe Resultat z.B. 1900
					Show-MsgBox "$Jahr ist KEIN Schaltjahr!" "Schaltjahr"
				}	
			} else {
				#Ausgabe Resultat z.B: 2004
				Show-MsgBox "$Jahr ist EIN Schaltjahr!" "Schaltjahr"
			}
		} else {
			#Ausgabe Resultat z.B: 1901
			Show-MsgBox "$Jahr ist KEIN Schaltjahr!" "Schaltjahr"
		}

		
		#Abfrage Wiederholung der Berechnung
		$eingabe = Show-MsgBox "Wollen Sie noch eine Berechnung machen?" "Eurorechner..." "Question" "YesNo" 
		$weiter = ($eingabe -eq "Yes") 
		
} While ($weiter) # wiederholen solange "Ja" gedrückt wurde...

write "Bye bye ..."