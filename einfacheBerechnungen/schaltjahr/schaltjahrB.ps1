do {
    #Vorbesetzung des Resultats
    $ergebnis = "??"
    $farbe = "yellow"
    Write-Host "`n*******  Schaltjahr-Rechner  ********"
    #Zahl von Benutzer einlesen
    [int]$zahl = Read-Host -Prompt "Bitte geben Sie eine Jahreszahl ein"

    #Berechnung �ber Modulo-Funktion (% -> gibt den "Rest" einer Division)
    if (  ($zahl % 4)  -eq  0) {
        if (  ($zahl % 100)  -eq  0) {
            if (  ($zahl % 400)  -eq  0) {
				# z.B. 2000
                $ergebnis = "EIN"
            } else {
				# z.B. 1900
                $ergebnis = "KEIN"
            }
        } else {
             $ergebnis = "EIN"
        }
    } else {
        $ergebnis = "KEIN"
    }
    #Ausgabe Ergebnis
    if ($ergebnis -eq "EIN") {
        $farbe = "green"
    }
    Write-Host "Das Jahr $zahl ist $ergebnis Schaltjahr!" -foregroundcolor $farbe
    
    #Weitermachen?
    $weiterMachen = Read-Host "`nWeitermachen?  [n]" 
} until ($weiterMachen.Equals("n"))