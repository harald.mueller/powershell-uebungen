﻿<#

Modul 403
Luca Raschetti
3.11.2017
Abschlussprojekt 
Mein erstes Quiz

#>

Write-Host      "****** Mein erstes Quiz ******"
""
write-host -nonewline "Möchten Sie das Quiz starten? (J/N)"
$starten = read-host
if ( $starten -ne "J" ) { exit }


Do{
    $Fragen = @(
       [pscustomobject]@{
           Question="Was ist die Hauptstadt der Schweiz?";
           Answers=[ordered]@{Zürich="Zürich";Basel="Basel";Bern="Bern";Genf="Genf";};
           CorrectAnswer="Bern"

           }
       [pscustomobject]@{
           Question="Was gibt 8*3?";
           Answers=[ordered]@{24="24";32="32";26="26";22="22";};
           CorrectAnswer="24"
       }
       [pscustomobject]@{
           Question="Welches ist das Flächenmässig grösste Land?";
           Answers=[ordered]@{USA="USA";China="China";Indien="Indien";Russland="Russland";};
           CorrectAnswer="Russland"
       } 
       [pscustomobject]@{
           Question="Wer gewann 2009/2010 die Champions League?";
           Answers=[ordered]@{Chelsea="Chelsea";Inter="Inter";Arsenal="Arsenal";Barcelona="Barcelona";};
           CorrectAnswer="Inter"
       }
       [pscustomobject]@{
           Question="Wer erbaute Machu Picchu?";
           Answers=[ordered]@{Azteken="Azteken";Sumerer="Sumerer";Inka="Inka";Römer="Römer";};
           CorrectAnswer="Inka"
       }
       [pscustomobject]@{
           Question="Seit welchem Jahr gibt es Pokemon?";
           Answers=[ordered]@{1996="1996";1988="1988";2001="2001";1998="1998";};
           CorrectAnswer="1996"
       }
       [pscustomobject]@{
           Question="Auf welcher Formel 1 Strecke werden die meisten Runden gefahren?";
           Answers=[ordered]@{Montreal="Montreal";Malaysia="Malaysia";Melbourne="Melbourne";Monaco="Monaco";};
           CorrectAnswer="Monaco"
       }   
) 

    $Fragen = $Fragen | Sort-Object {Get-Random}

    $fragenCount = 0
    $richtigCount = 0
    foreach($frage in $Fragen)
    {
        $frage.Question
        foreach($key in $frage.Answers.Keys)
        {
            "$key"
        }
        $antwort = Read-Host
        if($antwort -eq $frage.CorrectAnswer)
       {
            "Richtig!"
            clear
            $fragenCount++
            $richtigCount++
       }
       else
      {
           "Falsch!"
           clear
           $fragenCount++
       }
    }

    $prozent =  $richtigCount / $fragenCount  * 100
    
    ""
    
    "Von $fragenCount Fragen, hast du $richtigCount richtig beantwortet!"
    
    ""
    
    "Du hast $prozent%!"
    
    ""

    $Ende = Read-Host -Prompt "Möchtest du nochmals spielen?[J/N]"

}while($Ende -notlike "N")

Write-Host "Vielen Dank fürs Spielen"