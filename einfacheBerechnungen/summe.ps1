﻿### Einfache Berechnung ###
### .\summe.ps1 221 454 ###
# (Aufruf-)Parameterübergabe Anzeige
"`nWillkommen bei meiner Summenberechnung"
"`nParameter 1: " + [int]$args[0]
"Parameter 2: " + [int]$args[1]

# (Aufruf-)Parameterübergabe Zuweisung
[int]$z1 = $args[0] 
[int]$z2 = $args[1]

# Berchnung
[int]$summe = $z1 + $z2

# Ausgabe
"                  ---"
"Die Summe ist:     " + $summe

# Zusatzausgabe: das heutige Datum
(Get-Date).ToShortDateString()