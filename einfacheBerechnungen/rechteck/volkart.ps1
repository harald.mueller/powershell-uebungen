
[double]$ergebnis = 0
[double]$laenge = 0
[double]$breite = 0
[string]$nochmals = ""
do  {
    try {
        if ($laenge -eq 0) {$laenge = Read-Host -Prompt "Geben sie die Laenge eines Rechtecks ein."}
        if ($breite -eq 0) {$breite = Read-Host -Prompt "Geben Sie die Breite eines Rechtecks ein."}

        $ergebnis = $laenge*$breite
		$umfang   = 2*($laenge+$breite)
        if ($ergebnis -gt 0) {
            Write-Host "Ihr Rechteck hat:"
			Write-Host "Flaeche:   $ergebnis"
			Write-Host "Umfang:    $umfang"
			Write-Host "Diagonale: sqrt(($laenge*$laenge)+($breite*$breite))"
			""
        }

    } catch {
        Write-Host "`n Es wurde nichts berechnet."
        $nochmals = Read-Host " Noch ein Versuch? [j/n]"
    }

} until (($ergebnis -gt 0) -or ($nochmals -ne "j"))
