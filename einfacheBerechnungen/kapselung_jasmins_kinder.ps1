﻿cls
"================================"
$KindsName = "?"
$global:Jasmins_KinderAnzahl = 0
$Jasmins_Geschenk = "nix"

function Spitalaufenthalt() {    
    #Innerhalb einer Funktion gelten spezielle Regeln
    #wie zum Beispiel die "Kapselung" von Variabeln
    Write-Host "     -------- im Spital ----------"
    $Jasmins_KinderAnzahl++      #hier wird keine Zuweisung zum globalen Kontext gemacht
    $global:KindsName = "Kevin"  #hier wird  die  Zuweisung zum globalen Kontext gemacht
    $private:Jasmins_Geschenk = "Rote Tasche"
    Write-Host "S    Jasmins Kinder-Zahl = "$Jasmins_KinderAnzahl
    Write-Host "S    KindsName           = "$KindsName              -ForegroundColor:green
    Write-Host "S    Geschenk            = "$Jasmins_Geschenk       -ForegroundColor:red
    Write-Host "     -------- ende Spital --------"
}
Write-Host "A Jasmins Kinder-Zahl = "$Jasmins_KinderAnzahl
Write-Host "A KindsName           = "$KindsName                     -ForegroundColor:green
Write-Host "A Geschenk            = "$Jasmins_Geschenk              -ForegroundColor:yellow

Spitalaufenthalt  # Das ist der Funktionsaufruf. 

Write-Host "B Jasmins Kinder-Zahl = "$Jasmins_KinderAnzahl
Write-Host "B KindsName           = "$KindsName                     -ForegroundColor:green
Write-Host "B Geschenk            = "$Jasmins_Geschenk              -ForegroundColor:yellow
Write-Host "================================"
Write-Host ""

[int]$Jasmins_Alter = 21
Write-Host "C Jasmins_Alter  = "$Jasmins_Alter
[bool]$is_heute_JasminsGeburtstag = $TRUE
if ($is_heute_JasminsGeburtstag) {
    $Jasmins_Alter++
    Write-Host "C    Wenn Jasmin heute Geburtstag hat ist Jasmins_Alter = "$Jasmins_Alter
    Write-Host "C    Geschenk       = "$Jasmins_Geschenk            -ForegroundColor:yellow
}
Write-Host "C Jasmins_Alter  = "$Jasmins_Alter
Write-Host "C Geschenk       = "$Jasmins_Geschenk                   -ForegroundColor:yellow
Write-Host ""
