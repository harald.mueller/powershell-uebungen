﻿" A = "
" B = BMI-Berechnung"
" C = Notendurchschnitt berechnen"
$wahl = Read-Host " [A, B, C]"
switch ($wahl) {
    "A" {
            "hier kommt das für A"
        }

    "B" {
            [float]$kg = Read-Host "Geben Sie Ihr Gewicht in kg ein"
            [float]$gr = Read-Host "Geben Sie Ihre Körpergrosse in m ein"
            [float]$bmi = $kg / ($gr * $gr)
            "Ihr BMI ist "+[math]::Round($bmi,1)
        }

    "C" {
            [float]$n1 = Read-Host "Geben Sie die 1. Note ein"
            [float]$n2 = Read-Host "Geben Sie die 2. Note ein"
            [float]$n3 = Read-Host "Geben Sie die 3. Note ein"
            [float]$durchschnitt = ($n1 + $n2 + $n3) / 3
            "Der Durchschnitt ist (aufgerundet):  "+ [math]::Ceiling($durchschnitt)
            
        }
}