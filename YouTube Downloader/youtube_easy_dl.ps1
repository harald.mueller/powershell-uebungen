﻿# cd "C:\Users\haral\OneDrive\Technik\PS\Resource"
# $ziel = "C:\Users\haral\OneDrive\Technik\PS\data"
cd "C:\repo21\powershell\YouTube Downloader\Resource"
$ziel = "C:\repo21\powershell\YouTube Downloader\data"
$urlArray = `
  "https://www.youtube.com/watch?v=CU4toMzLDjU" `
, "https://www.youtube.com/watch?v=RzWB5jL5RX0" `


# $ErrorActionPreference = "SilentlyContinue"
$i = 0
foreach($url in $urlArray) {
    $zahl = "00$i"
    if ($i -gt 9)  {$zahl = "0$i"}
    if ($i -gt 99) {$zahl = "$i"}
    Write-Host "Download  $zahl  $url"
    .\youtube-dl.exe $url --format mp3 -q --no-playlist -o "$ziel\$zahl-%(title)s.%(ext)s"
    $i++
}

