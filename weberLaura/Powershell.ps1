﻿<#

Laura Weber
Modul 403
03.11.2017
Abschlussprojekt
Mein erstes Quiz

#>


Write-Host      "****** Mein erstes Quiz ******"
""
write-host -nonewline "Möchten Sie das Quiz starten? (J/N)"
$starten = read-host
if ( $starten -ne "J" ) { exit }


Do{
    $Fragen = @(
       [pscustomobject]@{
           Question="Wie hiess der Urkontinent?";
           Answers=[ordered]@{ Panama = "Panama"; Pangaea = "Pangaea"; Portugal = "Portugal"; Pangale = "Pangale";};
           CorrectAnswer="Pangaea"

           }
       [pscustomobject]@{
           Question="Was gibt 8*8?";
           Answers=[ordered]@{64="64";32="32";26="26";22="22";};
           CorrectAnswer="64"
       }
       [pscustomobject]@{
           Question="Welche ist die Hauptstadt von Großbritannien?";
           Answers=[ordered]@{Sydney = "Sydney"; Manchester = "Manchester"; London = "London"; Cambridge = "Cambridge";};
           CorrectAnswer="London"
       } 
       [pscustomobject]@{
           Question="Was heisst SBB ausgeschrieben?";
           Answers=[ordered]@{ SchweizerischeBundesBahn = "Schweizerische Bundes Bahn"; SiebeBuebeBrünzlet = "Siebe Buebe Brünzlet"; SchnelleBundesBahn = "Schnelle Bundes Bahn"; SchweizBahnBund = "Schweiz Bahn Bund";};
           CorrectAnswer="Schweizerische Bundes Bahn"
    
     
       }   
) 
 $Fragen = $Fragen | Sort-Object {Get-Random}

    $fragenCount = 0
    $richtigCount = 0
    foreach($frage in $Fragen)
    {
        $frage.Question
        foreach($key in $frage.Answers.Keys)
        {
            "$key"
        }
        $antwort = Read-Host
        if($antwort -eq $frage.CorrectAnswer)
       {
            "Richtig!"
            clear
            $fragenCount++
            $richtigCount++
       }
       else
      {
           "Falsch!"
           clear
           $fragenCount++
       }
    }

    $prozent =  $richtigCount / $fragenCount  * 100
    
    ""
    
    "Von $fragenCount Fragen, hast du $richtigCount richtig beantwortet!"
    
    ""
    
    "Du hast $prozent%!"
    
    ""

    $Ende = Read-Host -Prompt "Möchtest du nochmals spielen?[J/N]"

}while($Ende -notlike "N")

Write-Host "Bis zum nächsten Mal :)"