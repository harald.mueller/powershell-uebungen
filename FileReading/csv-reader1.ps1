﻿$filedata = Get-Content .\datafiles\rechnung21003.data

foreach($line in $filedata) {
    if ($line.StartsWith("Rechnung")) {
        echo "`n  verarbeite Rechnungszeile:"
        echo $line
    }
    if ($line.StartsWith("Herkunft")) {
        echo "`n  verarbeite Herkunftszeile:"
        echo $line
    }
    if ($line.StartsWith("Endkunde")) {
        echo "`n  verarbeite Endkundezeile:"
        echo $line
    }
    if ($line.StartsWith("RechnPos")) {
        echo "`n  verarbeite Rechnungspositionszeile:"
        echo $line
    }
}