﻿$filedata = Get-Content .\datafiles\rechnung*.data

$EK_pid = ""
$EK_nam = ""
$EK_adr = ""
$EK_ort = ""

$i = 1
$RP_Arr = @{}
$RP_Arr['bez'] = @{}
$RP_Arr['stk'] = @{}
$RP_Arr['ep'] = @{}
$RP_Arr['total'] = @{}

$rechnSumme = 0

foreach($line in $filedata) {
    
    if ($line.StartsWith("Rechnung")) {
        #echo "`n  verarbeite Rechnungszeile: $line"
    }
    
    if ($line.StartsWith("Herkunft")) {
        $sArr = $line.Split(";")
        #abfüllen der Zeilenelemente
        $HK_pid = $sArr[1]
        $HK_num = $sArr[2]
        $HK_nam = $sArr[3]
        $HK_adr = $sArr[4]
        $HK_ort = $sArr[5]
        $HK_mwst = $sArr[6]
        $HK_email = $sArr[7]
    }

    if ($line.StartsWith("Endkunde")) {
        # Line-Split in Kurzform, jedoch nicht ganz so effizient, 
        # weil der "split()" mehrmals gemacht wird, aber das ist heutzutage egal
        $EK_pid = $line.split(";")[1]
        $EK_nam = $line.split(";")[2]
        $EK_adr = $line.split(";")[3]
        $EK_ort = $line.split(";")[4]
    }
    
    if ($line.StartsWith("RechnPos")) {
        #echo "`n  Rechnungsposition: $line"
        $sArr = $line.Split(";")

        $RP_Arr['bez'][$i] = @{}
        $RP_Arr['bez'][$i] = $sArr[2]

        $RP_Arr['stk'][$i] = @{}
        $RP_Arr['stk'][$i] = $sArr[3]

        $RP_Arr['ep'][$i] = @{}
        $RP_Arr['ep'][$i] = $sArr[4]

        $RP_Arr['total'][$i] = @{}
        $RP_Arr['total'][$i] = $sArr[5]

        $RP_Arr.Summe += [double]$sArr[5]

        $i++
    }

} # End ForEach

$RP_Arr
"=Kontrollausgabe des Arrays"

echo ""
echo "Formattierte Positionszeilenausgabe:"
echo ""

#formattiert mit 2 Nachkommastellen
$formattierteSumme = "{0:f2}" -f $RP_Arr.Summe 

#Formattierung mit dem  -f  Parameter für Strings 
#in der schweiften Klammer ist vorne der String aus der Position nach dem Komma
#und hinten die Distanzangabe mit links auffüllen (Minuswerte machen linksbündig und rechts auffüllen)
"{0, 3} {1, -36} {2, 9} {3, 9} {4, 12}" -f "Pos", "Bezeichnung", "Anzahl", "Preis", "Total"
"-------------------------------------------------------------------------"

# Positionszeilen
$max = $RP_Arr["bez"].Count
for ($i=1; $i -le $max; $i++){
    $formattiertePosZeile = "{0, 3} {1, -36} {2, 6} {3, 12} {4, 12}" -f $i, $RP_Arr['bez'].$i, $RP_Arr['stk'].$i, $RP_Arr['ep'].$i, $RP_Arr['total'].$i
    $formattiertePosZeile
}
# Abschlusszeile
"{0, 60} {1, 12}" -f "-----------", "----------"
"{0, 60} {1, 12}" -f "Totalbetrag", $formattierteSumme


<#
#minimalistische Print-Ausgabe
"`n Daten Herkunft gespeichert:"
" PID : $HK_pid"
" KNR : $HK_num"
" NAME: $HK_nam"
" ADR : $HK_adr"
" ORT : $HK_ort"
" MWST: $HK_mwst"
" MAIL: $HK_email"

"`n Daten Endkunde gespeichert:"
" PID : $EK_pid"
" NAME: $EK_nam"
" ADR : $EK_adr"
" ORT : $EK_ort"
#>