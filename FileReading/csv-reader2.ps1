﻿$filedata = Get-Content .\datafiles\rechnung*.data

$EK_pid = ""
$EK_nam = ""
$EK_adr = ""
$EK_ort = ""

foreach($line in $filedata) {
    
    if ($line.StartsWith("Rechnung")) {
        #echo "`n  verarbeite Rechnungszeile:"
        #echo $line
    }
    
    if ($line.StartsWith("Herkunft")) {
        #echo "`n  verarbeite Herkunftszeile: $line"
        $sArr = $line.Split(";")
        #gibt ganzes Array auf Konsole heraus
        $sArr

        #abfüllen der Zeilenelemente
        $HK_pid = $sArr[1]
        $HK_num = $sArr[2]
        $HK_nam = $sArr[3]
        $HK_adr = $sArr[4]
    }

    if ($line.StartsWith("Endkunde")) {
        #echo "`n  verarbeite Endkundezeile: $line"
        #Line-Split in Kurzform
        $EK_pid = $line.split(";")[1]
        $EK_nam = $line.split(";")[2]
        $EK_adr = $line.split(";")[3]
        $EK_ort = $line.split(";")[4]
    }
    
    if ($line.StartsWith("RechnPos")) {
        #echo "`n  Rechnungsposition: $line"
    }

} # End ForEach

#minimalistische Print-Ausgabe
"`n Daten Herkunft gespeichert:"
" PID : $HK_pid"
" KNR : $HK_num"
" NAME: $HK_nam"

"`n Daten Endkunde gespeichert:"
" PID : $EK_pid"
" NAME: $EK_nam"
" ADR : $EK_adr"
" ORT : $EK_ort"
