[double]$flaeche = 0
[double]$laenge = 0
[double]$breite = 0
[string]$nochmals = ""
do  {
    try {
		Write-Host "`nRechteck-Daten Berechnung`n======================="
        if ($laenge -eq 0) {$laenge = Read-Host -Prompt "Laenge in cm "}
        if ($breite -eq 0) {$breite = Read-Host -Prompt "Breite in cm "}

        $flaeche = $laenge * $breite
        if ($flaeche -gt 0) {
			$umfang   = 2*($laenge+$breite)
			$diagonale = [math]::sqrt( ($laenge*$laenge)+($breite*$breite) )
			$diagonale = [math]::round($diagonale, 2)
            "Das Rechteck hat" 
			Write-Host "    Flaeche:   $flaeche cm2"
			Write-Host "    Umfang:    $umfang cm"
			Write-Host "    Diagonale: $diagonale cm"
			""
        }
    } catch {
        Write-Host "`n Es wurde nichts berechnet."
        $nochmals = Read-Host " Noch ein Versuch? [j/n]"
    }
} until (($flaeche -gt 0) -or ($nochmals -ne "j"))
