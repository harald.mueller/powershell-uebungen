#Eingelesener Pfad ausgeben
# a) nur Ordner und Unterordner
# b) nur Dateien (aus Unterordnern) nach Endungen sortiert  
##########################################################

# Script: Kap. ShowPath2.ps1
# Autor: KEL
# Datum:21.10.12
# Version: 1.0

If ($Args.count �ne 1) {
    write-warning "Call the script with 1 parameter: path`nExample: powershell.exe .\ShowPath2.ps1 H:\`n         => Args[0] = H:\"
} else { 
    # Pfad vorhanden?
    If (test-path $Args[0]) { 
		#Konsole s�ubern
		CLS
		
		#Alle Ordner mit Unterordner und Dateien auslesen
		$all = Get-ChildItem $Args[0]  -recurse
		# Write-Host $all # only for debug
		
			# a) Ordner und Unterordner ausgeben
			Write-Host -foregroundcolor "red" "`nSUB-DIRS of <$Args>:`n=================================`n"
			#Ordner und Unterordner ausgeben
			foreach ($p in $all) {
			  if ($p.mode -match "d") {
			    Write-host -f "red" $p.FullName
			  }
		    }
			
			# b) ALLE Dateien sortiert nach Extensions ausgeben
			$all = $all | sort extension
			Write-Host -foregroundcolor "green" "`nFILES and SUB-FILES of <$Args>:`n=================================`n"
			#Ordner und Unterordner ausgeben
			foreach ($p in $all) {
			  if ($p.mode -notmatch "d") {
			    Write-host -f "green" $p.FullName
			  }
		    }	
			
	} else { 
		    Write-Warning "Path <$Args> does not exists!"  
	} 
}