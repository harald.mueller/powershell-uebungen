﻿#RSS-Feed runterladen
#######################################################

# Script: dNet6_21.ps1
# Autor: KEL
# Datum:9.12.09
# Version: 1.0
# Version: 1.1 22.10.12 Proxy-Parameter für optionale Proxy-Einbindung

#.Net Objekte initialisieren
$web = new-object -typename Net.WebClient

	#V1.1: Optionaler Proxy via Parameter ein/ausschalten
	if ($args[0] -match "proxy") { 
	  $proxy = New-Object System.Net.WebProxy("http://10.1.200.139:8080")
	  $proxy.UseDefaultCredentials = $true
	  $web.proxy = $proxy
	}
	
$page = $web.DownloadString("http://www.tagesanzeiger.ch")
$page > h:\tagi.html

# Zusatz: ie starten mit HTML-Datei
$ie = new-object -comobject InternetExplorer.Application
$ie.navigate2("h:\tagi.html")
$ie.Document.title = "TagiNews:"
$ie.visible = $true