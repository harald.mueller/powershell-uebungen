﻿# The scripts starts here. First check: do we have 3 parameters? 
If ($Args.count –ne 3) {
    write-warning "call the script with 3 parameter: source target date-in-months"
} else { 
    If (test-path $Args[0]) { 
	    If (test-path $Args[1] ) { 
		    $CheckDate = ((Get-date).AddMonths(-$Args[2])).ToOADate()
			$MyPath = $Args[1]
			Dir $args[0] -recurse | where-object {$_.mode -notmatch "d"} | foreach-object {
			    
				if ( ($_.LastAccessTime).ToOADate() -lt $CheckDate) {
				  
					 Copy-Item $_.FullName $MyPath
					 
				}
				
			} 
	    } else { 
		    Write-Warning "Target path does not exists"  
		} 
	} else {
	    Write-Warning ("Source path does not exists: " + $Args[0])
	}
}