#Funktion InputBox
##################

# Script: InputBox.ps1 ["Eingabetext"] ["Fenstertitel"]
# Einfache Texteingabe mit Formular und Abbruchmöglichkeiten
# Rückgabewerte: Eingabetext bei [OK]
#                "Cancel" bei [Cancel] oder [X]
# Autor: KEL
# Version 1.0: 25.10.12
# Version 1.1: 20.03.15 $script:answer wegen Gültigkeitsbereich eingefügt

#.Net Bibliothek Forms laden
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 

# Antwort für Abbruch presetten 
$script:answer="Cancel"

[Object] $Form = New-Object System.Windows.Forms.Form 
if ($args.Count -gt 1) {
   $Form.Text = $args[1]
} else {  
   $Form.Text = "PS-InputBox"
}
$Form.Size = New-Object System.Drawing.Size(300,200) 
$Form.StartPosition = "CenterScreen"


#Ok-Button
$OKButton = New-Object System.Windows.Forms.Button
$OKButton.Location = New-Object System.Drawing.Size(75,120)
$OKButton.Size = New-Object System.Drawing.Size(75,23)
$OKButton.Text = "Ok"
$OKButton.Add_Click({$script:answer=$TextBox.Text;$Form.Close()}) #Eingabe aus TextBox in $script:answer speichern
$Form.Controls.Add($OKButton)

#Cancel-Button
$CancelButton = New-Object System.Windows.Forms.Button
$CancelButton.Location = New-Object System.Drawing.Size(150,120)
$CancelButton.Size = New-Object System.Drawing.Size(75,23)
$CancelButton.Text = "Cancel"
$CancelButton.Add_Click({$script:answer="Cancel";$Form.Close()})
$Form.Controls.Add($CancelButton)

#Anzeigetext
$Label = New-Object System.Windows.Forms.Label
$Label.Location = New-Object System.Drawing.Size(10,20) 
$Label.Size = New-Object System.Drawing.Size(280,20) 
if ($args.count -gt 0) {
    $Label.Text = $args[0]
} else {  
	$Label.Text = "Input:"
}
$Form.Controls.Add($Label) 

#Textbox
$TextBox = New-Object System.Windows.Forms.TextBox 
$TextBox.Location = New-Object System.Drawing.Size(10,40) 
$TextBox.Size = New-Object System.Drawing.Size(260,60) 
$Form.Controls.Add($TextBox) 

#Tastenaktionen einfügen
$Form.KeyPreview = $True
$Form.Add_KeyDown({if ($_.KeyCode -eq "Enter") 
    {$script:answer=$TextBox.Text;$Form.Close()}}) #Enter ist wie [OK]
$Form.Add_KeyDown({if ($_.KeyCode -eq "Escape") 
    {$script:answer="Cancel";$Form.Close()}}) #ESC ist wie [X]
	
#Initialisirung und starten des Formulars
$Form.Topmost = $True
$Form.Add_Shown({$Form.Activate()})
#Falls Abbruch mit [X]...
$Form.ShowDialog() | out-null

#Rückgabe des Eingabewertes
return $script:answer
