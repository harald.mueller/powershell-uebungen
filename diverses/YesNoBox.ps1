#Funktion YesNoBox
##################

# Script: YesNoBox.ps1 ["Ausgabetext"] ["Fenstertitel"]
# Funktion zur einfachen Eingabe von JA/Nein-Abfragen! Grösse evtl. anpassen!
# Rückgabewerte: "_Yes" bei [YES] oder 
#                "_No" bei [NO] 
#           oder "_Cancel" bei [X]
# Autor: KEL
# Version 1.0: 25.10.12 für MP-Vorbereitung Übung 2
# Version 1.1: 23.03.15 $script:answer eingefügt wegen neuem Gültigkeitsbereich

#.Net Bibliothek Forms laden
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
[void][reflection.assembly]::LoadWithPartialName("System.Windows.Forms")

#.Net Objekte initialisieren
$form = new-object Windows.Forms.Form
$Form.Size = New-Object System.Drawing.Size(300,200) 
$Form.StartPosition = "CenterScreen"

#Titel
if ($args.Count -gt 1) {
   $Form.Text = $args[1]
} else {  
   $Form.Text = "PS-YesNoBox"
}

#Neuer No-Button:
$No = new-object Windows.Forms.Button
$No.Text = "Nein"
$No.Left = 100 #Position innerhalb des Formulars
$No.Top = 100
$No.Width = 50
$No.Height = 25

#Click-Aktion dem Button beifügen  
$No.Add_Click( {$script:answer = "_No"; $form.close()} )

#Button dem Formular beifügen mit 
$form.Controls.Add($No)

#Neuer Yes-Button:
$yes = new-object Windows.Forms.Button
$yes.Text = "Ja"
$yes.Left = 50
$yes.Top = 100
$yes.Width = 50
$yes.Height = 25
#Click-Aktion dem Button beifügen  
$yes.Add_Click( {$script:answer = "_Yes"; $form.close()} )
#Button dem Formular beifügen mit 
$form.Controls.Add($yes)

#Text (Label) platzieren:
$Label = New-Object System.Windows.Forms.Label
$Label.Left = 50
$Label.Top = 50
$Label.Width = 300
$Label.Height = 60
if ($args.count -gt 0) {
    $Label.Text = $args[0]
} else {  
	$Label.Text = "Sind Sie sicher?"
}
$Form.Controls.Add($Label) 

#Tastenaktionen einfügen
$Form.KeyPreview = $True
$Form.Add_KeyDown({if ($_.KeyCode -eq "J") 
    {$answer = "_Yes"; $Form.Close()}}) #Beendet MsgBox: Rückkehr zum Aufruferscript
$Form.Add_KeyDown({if ($_.KeyCode -eq "N") 
	{$answer = "_No";$Form.Close()}}) 
	

#Formular starten
$script:answer = "_Cancel"
$form.ShowDialog() | Out-Null

#Rückgabe der Tasteneingabe "_Yes" oder "_No"
return $script:answer