﻿function get-freememory ([string]$units = "b") { # ______
  $comp = get-wmiobject Win32_OperatingSystem
  $mem = $comp.FreePhysicalMemory + $comp.FreeVirtualMemory
  switch ($units) {
    "b" {return $mem} # ______________
    "k" {return [int]($mem / 1024)}
    "m" {return [int](($mem * 100) / 1048576) / 100}
  }
}

get-freememory m
get-freememory k
get-freememory b