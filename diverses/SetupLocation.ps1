﻿# Wechselt PS-Pfad zu aktuellem Script-Pfad
###########################################
# Autor: Kel
# Version 1.0: 25.10.12
# Anwendung: 
# 1) Speichern Sie das Script in den Scriptordner, wo ihre anderen Scripts zum starten sind
# 2) Ziehen Sie das Script von Dort via Drag-and-drop ins Powershellfenster und starten Sie es
# 3) Der Pfad im Powershellfenster ist nun in ihrem Scriptordner

# Aktueller Scriptpfad einlesen und als akt. Pfad setzen
[String] $scriptpath = Split-Path $MyInvocation.MyCommand.Path
write $scriptpath
Set-Location $scriptpath