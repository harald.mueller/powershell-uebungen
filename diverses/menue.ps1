﻿#Menü
#####

# Script: Menue.ps1
# Autor: KEL
# Datum:19.11.09
# Version: 1.0

#Variable initialisieren
$eingabe = ""

cls
do {
  #Menüausgabe
  #cls
  Write-Host "`n  Menue:`n  ======="
  Write-Host "  [1] Menüpunkt 1"
  Write-Host "  [2] Menüpunkt 2"
  Write-Host "  [3] Menüpunkt 3"
  Write-Host "  [0] Ende"
  $eingabe = Read-Host "`n   --> Geben Sie bitte Ihre Wahl an: [0-3]"

  # Menüwahl ausführen
  switch ($eingabe) {
	1 {
	  Write-Host "Ihre Eingabe ist 1..." 	
	}
	2 {
	  Write-Host "Ihre Eingabe ist 2..."
	}
	3 {
      Write-Host "Ihre Eingabe ist 3..." 
	}
	0 {
	   # tue nichts --> Abbruch	
	}
	default {
	   # Fehler
	   Write-Host "Ihre Eingabe ist falsch! Bitte nochmals..."
	}
}
# Beende Menü wenn "0" eingegeben wurde
} until ($eingabe -eq "0")

write-host Bye-bye