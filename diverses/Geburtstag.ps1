﻿# Geburtstage vergeleichen
##########################
# Autor: Kel
# Version 1.0: 12.11.12 
# Funktion: 2 Geburtstage vergeleichen
# Eingabe über Inputbox.ps1 mit Fehlerabfrage und Verarbeitungsloop
# Ausgabe über Show-MsgBox-Funktion, die vorab geladen wird
# Übung 2 für Struktogrammübungen

# Konstanten-Deklaration:

# Variablen-Deklaration und Initialisation:
[string] $eingabe = ""  	# für Inputbox
[single] $jahr1 = 0		    # speichert Jahreszahl Person 1
[single] $jahr2 = 0		    # speichert Jahreszahl Person 2
[single] $monat1 = 0		# speichert Monatszahl Person 1
[single] $monat2 = 0		# speichert Monatszahl Person 2
[single] $tag1 = 0		    # speichert Tageszahl Person 1
[single] $tag2 = 0		    # speichert Tageszahl Person 2
[bool]   $weiter = $true	# für Ablaufsteuerung
[String] $scriptpath = Split-Path $MyInvocation.MyCommand.Path
							# aktueller Scriptpfad korrekt initialisieren
# Wechsle zu lokalem Script-Verzeichnis
Set-Location $scriptpath							

#Externe Funktion Show-MsgBox einbinden
. .\Show-MsgBox.ps1

# Hauptschleife: ganze Aufgabe wiederholen
Do {
        # Eingabewiederholung bei Falscheingaben Person 1
		Do {
		    #Setzt Flag für neue Eingabe auf "weiter"
		    $weiter = $true
			
			#Eingabe Tag1 als Text
			$eingabe = .\Inputbox.ps1 "Geben Sie den GeburtsTAG der ersten Person ein:" "Geburtstag P1"			
			if ($eingabe -eq "Cancel") {
			    write "Programm abgebrochen..."
				exit
			}
			#Umwandlung Text in Zahl (Double) ohne Abbruch bei Fehler (=0)
			$tag1 = $eingabe -as [double] #Siehe Kapitel 3
			#Eingabeüberprüfung (Robustheit)
			if ($tag1 -lt 1 -or $tag1 -gt 31){ 
			   Show-MsgBox "Falsche Tag-Eingabe: Bitte 2-stellige Zahl eingeben" "ERROR!" "Exclamation"
			   $weiter = $false
			}
		} Until ($weiter)	
		Do {
		    #Setzt Flag für neue Eingabe auf "weiter"
		    $weiter = $true
			
			#Eingabe Monat1 als Text
			$eingabe = .\Inputbox.ps1 "Geben Sie den GeburtsMONAT der ersten Person ein:" "Geburtstag P1"			
			if ($eingabe -eq "Cancel") {
			    write "Programm abgebrochen..."
				exit
			}
			#Umwandlung Text in Zahl (Double) ohne Abbruch bei Fehler (=0)
			$monat1 = $eingabe -as [double] #Siehe Kapitel 3
			#Eingabeüberprüfung (Robustheit)
			if ($monat1 -lt 1 -or $monat1 -gt 12){ 
			   Show-MsgBox "Falsche Monat-Eingabe: Bitte 2-stellige Zahl eingeben" "ERROR!" "Exclamation"
			   $weiter = $false
			}
		} Until ($weiter)
		Do {
		    #Setzt Flag für neue Eingabe auf "weiter"
		    $weiter = $true
			
			#Eingabe Jahr1 als Text
			$eingabe = .\Inputbox.ps1 "Geben Sie das GeburtsJAHR der ersten Person ein:" "Geburtstag P1"			
			if ($eingabe -eq "Cancel") {
			    write "Programm abgebrochen..."
				exit
			}
			#Umwandlung Text in Zahl (Double) ohne Abbruch bei Fehler (=0)
			$jahr1 = $eingabe -as [double] #Siehe Kapitel 3
			#Eingabeüberprüfung (Robustheit)
			if ($jahr1 -lt 1 -or $jahr1 -gt 12){ 
			   Show-MsgBox "Falsche Eingabe: Bitte 4-stellige Zahl eingeben" "ERROR!" "Exclamation"
			   $weiter = $false
			}
		} Until ($weiter)

        # Eingabewiederholung bei Falscheingaben Person 2
		Do {
		    #Setzt Flag für neue Eingabe auf "weiter"
		    $weiter = $true
			
			#Eingabe Tag2 als Text
			$eingabe = .\Inputbox.ps1 "Geben Sie den GeburtsTAG der ersten Person ein:" "Geburtstag P2"			
			if ($eingabe -eq "Cancel") {
			    write "Programm abgebrochen..."
				exit
			}
			#Umwandlung Text in Zahl (Double) ohne Abbruch bei Fehler (=0)
			$tag2 = $eingabe -as [double] #Siehe Kapitel 3
			#Eingabeüberprüfung (Robustheit)
			if ($tag2 -lt 1 -or $tag2 -gt 31){ 
			   Show-MsgBox "Falsche Tag-Eingabe: Bitte 2-stellige Zahl eingeben" "ERROR!" "Exclamation"
			   $weiter = $false
			}
		} Until ($weiter)	
		Do {
		    #Setzt Flag für neue Eingabe auf "weiter"
		    $weiter = $true
			
			#Eingabe Monat2 als Text
			$eingabe = .\Inputbox.ps1 "Geben Sie den GeburtsMONAT der ersten Person ein:" "Geburtstag P2"			
			if ($eingabe -eq "Cancel") {
			    write "Programm abgebrochen..."
				exit
			}
			#Umwandlung Text in Zahl (Double) ohne Abbruch bei Fehler (=0)
			$monat2 = $eingabe -as [double] #Siehe Kapitel 3
			#Eingabeüberprüfung (Robustheit)
			if ($monat2 -lt 1 -or $monat2 -gt 12){ 
			   Show-MsgBox "Falsche Monat-Eingabe: Bitte 2-stellige Zahl eingeben" "ERROR!" "Exclamation"
			   $weiter = $false
			}
		} Until ($weiter)
		Do {
		    #Setzt Flag für neue Eingabe auf "weiter"
		    $weiter = $true
			
			#Eingabe Jahr2 als Text
			$eingabe = .\Inputbox.ps1 "Geben Sie das GeburtsJAHR der ersten Person ein:" "Geburtstag P2"			
			if ($eingabe -eq "Cancel") {
			    write "Programm abgebrochen..."
				exit
			}
			#Umwandlung Text in Zahl (Double) ohne Abbruch bei Fehler (=0)
			$jahr2 = $eingabe -as [double] #Siehe Kapitel 3
			#Eingabeüberprüfung (Robustheit)
			if ($jahr2 -lt 1 -or $jahr2 -gt 12){ 
			   Show-MsgBox "Falsche Eingabe: Bitte 4-stellige Zahl eingeben" "ERROR!" "Exclamation"
			   $weiter = $false
			}
		} Until ($weiter)

		#---Schaltjahr berechnen---
		if ($jahr1 -eq $jahr2) {

			# Jahr ist gleich:
			if ($monat1 -eq $monat2) {
			
				# Jahr und Monat sind gleich:
				if ($tag1 -eq $tag2) {
				
					# Jahr und Monat und Tage sind gleich:
					Show-MsgBox "P1 gleich alt wie P2" "Vergleich"
					
				} elseif($tag1 -lt $tag2) {
					#Ausgabe Resultat
					Show-MsgBox "P1 ist älter als P2" "Vergleich"	
				} else {
					#Ausgabe Resultat z.B: 1901
					Show-MsgBox "P1 ist jünger als P2" "Vergleich"	
				}
				
			} elseif($monat1 -lt $monat2) {
				#Ausgabe Resultat
				Show-MsgBox "P1 ist älter als P2" "Vergleich"	
			} else {
				#Ausgabe Resultat z.B: 1901
				Show-MsgBox "P1 ist jünger als P2" "Vergleich"	
			}
			

		} elseif($jahr1 -lt $jahr2) {
			#Ausgabe Resultat
			Show-MsgBox "P1 ist älter als P2" "Vergleich"	
		} else {
			#Ausgabe Resultat z.B: 1901
			Show-MsgBox "P1 ist jünger als P2" "Vergleich"	
		}

		
		#Abfrage Wiederholung der Berechnung
		$eingabe = Show-MsgBox "Wollen Sie noch eine Berechnung machen?" "Eurorechner..." "Question" "YesNo" 
		$weiter = ($eingabe -eq "Yes") 
		
} While ($weiter) # wiederholen solange "Ja" gedrückt wurde...

write "Bye bye ..."