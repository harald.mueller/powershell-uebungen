﻿#Erstellt Formular mit OK-Button
################################

# Script: dNet6_3.ps1
# Autor: KEL
# Datum:9.12.09
# Version: 1.0

#.Net Bibliothek Forms laden
[void][reflection.assembly]::LoadWithPartialName("System.Windows.Forms")

#.Net Objekte initialisieren
$form = new-object Windows.Forms.Form
$Form.Size = New-Object System.Drawing.Size(200,200) 
$Form.StartPosition = "CenterScreen"
$answer = "Exit"

#Titel
$form.Text = "Beispielfenster"

#Neuer Cancel-Button:
$Cbutton = new-object Windows.Forms.Button
$Cbutton.Text = "Cancel"
$Cbutton.Left = 100 #Position innerhalb des Formulars
$Cbutton.Top = 100
$Cbutton.Width = 50
$Cbutton.Height = 25

#Click-Aktion dem Button beifügen  
$Cbutton.Add_Click( {$answer = "Cancel"; $form.close()} )

#Button dem Formular beifügen mit 
$form.Controls.Add($Cbutton)

#Formular starten
$form.ShowDialog() | out-null


#Abfrage der Benutzeeingabe
if ($answer -eq "Cancel") {Write-Host Sie haben Cancel gedrückt}
if ($answer -eq "Exit") {Write-Host Sie haben [X] gedrückt}