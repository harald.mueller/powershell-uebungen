﻿#Bewegt alte Dateien von Quelle zum Ziel
########################################

# Script: Kap. 5.3.2 MoveOld.ps1 Quelle Ziel AnzMonate
# Autor: KEL
# Datum:16.12.09
# Version: 1.0
# Version: 1.1 Helptext verbessert

# Überprüfung:  3 Argumente übergeben? 
If ($Args.count –ne 3) {
    write-warning "Call the script with 3 parameters: source target date-in-months`nExample: powershell.exe .\MoveOld.ps1 . D:\ 2`n         => Args[0] = . (lokal dir),  Args[1] = D:\,  Args[2] = 2 months"
} else { 
    # Quelle vorhanden?
    If (test-path $Args[0]) { 
	    # Ziel vorhanden?
	    If (test-path $Args[1] ) { 
		    # Datum vor AnzMonate generieren
		    $CheckDate = ((Get-date).AddMonths(-$Args[2])).ToOADate()
			# Ziel zwischenspeichern --> Objekt
			$Ziel = $Args[1]
			Dir $args[0] -recurse | where-object {$_.mode -notmatch "d"} | foreach-object {
			    
				if ( ($_.LastAccessTime).ToOADate() -lt $CheckDate) {
				  
					 #Liste ausgeben:
			         $NeuerName = $Ziel + "\" + $_.name
			         #Write-Host ohne Klammern --> ohne +
		             write-host $_.fullname "--> " $NeuerName  ": " $_.LastAccesstime

				     # Move mit Rechten:
				     $myACL = get-Acl $_.FullName
					 Move-Item $_.FullName $Ziel 
					 Set-Acl $NeuerName $myACL
					 
				}
				
			} 
	    } else { 
		    Write-Warning "Target path does not exists"  
		} 
	} else {
	    Write-Warning ("Source path does not exists: " + $Args[0])
	}
}