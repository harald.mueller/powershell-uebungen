#Erstellt Formular mit Ok und Cancel-button
############################################

# Script: OkCancel.ps1
# Autor: KEL
# Version 1.0: 17.12.09 
# Version 1.1: 24.10.12 Variablen mit Typendeklaration eingefügt für MP-Vorbereitung Übung 1
# Version 1.2: 20.03.15 $script: wegen neuem Gültigkeitsbereich eingefügt

#.Net Bibliothek Forms laden
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 

[Object] $Form = New-Object System.Windows.Forms.Form 
$Form.Text = "Eingabeformular"
$Form.Size = New-Object System.Drawing.Size(300,200) 
$Form.StartPosition = "CenterScreen"


#Ok-Button
$OKButton = New-Object System.Windows.Forms.Button
$OKButton.Location = New-Object System.Drawing.Size(75,120)
$OKButton.Size = New-Object System.Drawing.Size(75,23)
$OKButton.Text = "Ok"
$OKButton.Add_Click({$script:answer=$TextBox.Text;$Form.Close()}) #Eingabe aus TextBox in $Antwort speichern
$Form.Controls.Add($OKButton)

#Cancel-Button
$CancelButton = New-Object System.Windows.Forms.Button
$CancelButton.Location = New-Object System.Drawing.Size(150,120)
$CancelButton.Size = New-Object System.Drawing.Size(75,23)
$CancelButton.Text = "Cancel"
$CancelButton.Add_Click({$script:answer="Cancel"; $Form.Close()})
$Form.Controls.Add($CancelButton)

#Anzeigetext
$Label = New-Object System.Windows.Forms.Label
$Label.Location = New-Object System.Drawing.Size(10,20) 
$Label.Size = New-Object System.Drawing.Size(280,20) 
$Label.Text = "Bitte geben Sie ihren Namen ein:"
$Form.Controls.Add($Label) 

#Textbox
$TextBox = New-Object System.Windows.Forms.TextBox 
$TextBox.Location = New-Object System.Drawing.Size(10,40) 
$TextBox.Size = New-Object System.Drawing.Size(260,20) 
$Form.Controls.Add($TextBox) 


#Tastenaktionen einfügen
$Form.KeyPreview = $True
$Form.Add_KeyDown({if ($_.KeyCode -eq "Enter") 
    {$script:answer=$TextBox.Text;$Form.Close()}})
$Form.Add_KeyDown({if ($_.KeyCode -eq "Escape") 
    {$script:answer="Escape"; $Form.Close()}})
	
#Initialisirung und starten des Formulars
$Form.Topmost = $True
$Form.Add_Shown({$Form.Activate()})
[String] $script:answer="Exit"
$Form.ShowDialog() |Out-Null

#Abfrage der Benutzeeingabe nach Beenden des Formulars
if ($script:answer -eq "Escape") {Write-Host Sie haben ESC gedrückt}
else {if ($script:answer -eq "Exit") {
             Write-Host Sie haben [X] gedrückt}
      else {if ($script:answer -eq "Cancel") {
	         Write-Host Sie haben Cancel gedrückt
	       } else {
	        # TextBox-Eingabe anzeigen bei OK
		     Write-Host Sie haben $script:answer eingegeben 
	    }
	}
}