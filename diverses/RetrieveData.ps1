﻿function GetMem { 
  $temp = Get-WmiObject "Win32_OperatingSystem" -computername $args[0]
  $MemInBytes = $temp.MaxProcessMemorySize / 1Mb
  Write-Output $MemInBytes
}

function GetNumCPU ($cname) { 
  $temp = Get-WmiObject "Win32_ComputerSystem" -computername $cname 
  $nocpu = $temp.NumberOfProcessors
  return $nocpu 
}

function RetrieveData {
  $tmpNoCPU = GetNumCPU $args[0] 
  $tmpMemInMBytes = GetMem $args[0] 
  $computername = $args[0] 
  Write-Output "The Computer $computername has $tmpNoCPU CPU(s) and $tmpMemInMBytes Mbytes of RAM installed."
}

RetrieveData "localhost"