﻿#BMI-Rechner
##################

# Berechnet den BMI anhand der Eingabedaten Grösse & Gewicht
# Ein Formular wird erstellt um die Eingabedaten zu werfassen. Mit dem Button Abbrechen kann die 
# Verarbeitung beendet werden.
# Mit dem Button Berechnen wird der BMI mit der Ansalyse im Formular ausgegeben und weitere Eingabedaten können eingegeben werden!
#
# Autor: KEL
# Version 1.0: 29.10.12
# Version 1.1: 31.10.12 Show-Msgbox implemented
# Version 1.2:  5.11.12 Minor issues with $result and $strKlasse solved

#.Net Bibliothek Forms laden
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 

#============== Variablen Deklaration und Initialisiation ================
[Single] $sngGewicht = 0 # Gewicht in kg 
[Single] $sngGroesse = 0 # Grösse in m
[single] $sngBMI = 0	 # BMI Wert
[string] $strKlass = ""  # Klassifizierung
[string] $result = ""    # Ausgabetext mit BMI und Klassifizierung
#
#Fenster Raster 
[Int32] $fBreite  = 270 #Fenster Breite
[Int32] $fAbstand =  30 #Standard Abstand Raster
[Int32] $fHoehe   =  20 #Button Höhe  
#
# Aktueller Scriptpfad einlesen und als akt. Pfad setzen
[String] $scriptpath = Split-Path $MyInvocation.MyCommand.Path						
Set-Location $scriptpath

#Einbinden der Show-Msgbox Funktion
. .\Show-MsgBox.ps1

#============== BMI Funktion ==============================================
function berechnen () {

 # Eingabewerte in korrekten Typ umwandeln; falls Fehleingabe wird Resultat 0
 $sngGewicht = $Tb_Gewicht.Text -as [single]
 $sngGroesse = $Tb_Groesse.Text -as [single]

 #Fehlerabfrage der drei Eingaben: Anzeige des Fehlers und beenden der Berechnung
 if (($Tb_Gesch.Text -ne "m") -and ($Tb_Gesch.Text -ne "w")) {
   Show-MsgBox "Bitte korrektes Geschlecht eingeben!" " FEHLER!!" "Exclamation"
   return
 }
 if ($sngGewicht -lt 20) {
   Show-MsgBox "Bitte korrektes Gewicht eingeben!" " FEHLER!!" "Exclamation"
   return
 }
  if ($sngGroesse -lt 0.5) {
   Show-MsgBox "Bitte korrekte Grösse eingeben!" " FEHLER!!" "Exclamation"
   return
 }
 
 #BMI Berechnen und runden
 $sngBMI = $sngGewicht / ($sngGroesse * $sngGroesse)
 $sngBMI =[Math]::Round($sngBMI,1)
 
 #Klassierung bestimmen
 if ($Tb_Gesch.Text -eq "m") {
  	#Männlich:
  	if ($sngBMI -lt 20) {
	 	$strKlass = "Untergewicht!"
	} else { if ($sngBMI -lt 25) {
	 	$strKlass = "Normalgewicht!"
	} else {if ($sngBMI -lt 30) {
	 	$strKlass = "Übergewicht!"
	} else {if ($sngBMI -lt 40) {
	 	$strKlass = "ADIPOSITAS!"
	} else {
	    $strKlass = "MASSIVE ADIPOSITAS!"
	}}}}
 } else {
  	#Weiblich:
  	if ($sngBMI -lt 19) {
	 	$strKlass = "Untergewicht!"
	} else { if ($sngBMI -lt 24) {
	 	$strKlass = "Normalgewicht!"
	} else {if ($sngBMI -lt 30) {
	 	$strKlass = "Übergewicht!"
	} else {if ($sngBMI -lt 40) {
	 	$strKlass = "ADIPOSITAS!"
	} else {
	    $strKlass = "MASSIVE ADIPOSITAS!"
	}}}}
 }
 
  #BMI und Klassifizierung ins Formular schreiben
 $BMI.Text = "BMI: $sngBMI"
 $Klass.Text = "> $strKlass"
 
 # Resultat auf Konsole ausgeben
 $result = $BMI.Text + " (" + $Tb_Gesch.Text + ")`n" + $Klass.Text + "`n"
 Write-Host $result
}


#=============== Fenster aufbauen =========================================
[Object] $Form = New-Object System.Windows.Forms.Form 
$Form.Text = "BMI-Rechner"
#Fenstergrösse 9 plus 2 Zeilen hoch
$Form.Size = New-Object System.Drawing.Size($fBreite, ((9+2)*$fAbstand)) 
$Form.StartPosition = "CenterScreen"

#Anzeigetext Gschlecht 1.Zeile
$Gesch = New-Object System.Windows.Forms.Label
$Gesch.Location = New-Object System.Drawing.Size($fAbstand, (1*$fAbstand)) 
$Gesch.Size = New-Object System.Drawing.Size(($fBreite - 2*$fAbstand),$fHoehe)  
$Gesch.Text = "Geschlecht: Bitte m oder w eingeben!"
$Form.Controls.Add($Gesch) 

#Textbox Geschlecht 2.Zeile
$Tb_Gesch = New-Object System.Windows.Forms.TextBox 
$Tb_Gesch.Location = New-Object System.Drawing.Size($fAbstand, (2*$fAbstand)) 
$Tb_Gesch.Size = New-Object System.Drawing.Size(40,$fHoehe) 
$Tb_Gesch.Text = "m" #Vorgabe
$Form.Controls.Add($Tb_Gesch) 

#Anzeigetext Gewicht
$Gewicht = New-Object System.Windows.Forms.Label
$Gewicht.Location = New-Object System.Drawing.Size($fAbstand, (3*$fAbstand)) 
$Gewicht.Size = New-Object System.Drawing.Size(($fBreite - 2*$fAbstand),$fHoehe)  
$Gewicht.Text = "Gewicht in kg:"
$Form.Controls.Add($Gewicht) 

#Textbox Gewichteingabe
$Tb_Gewicht = New-Object System.Windows.Forms.TextBox 
$Tb_Gewicht.Location = New-Object System.Drawing.Size($fAbstand, (4*$fAbstand)) 
$Tb_Gewicht.Size = New-Object System.Drawing.Size(100,$fHoehe) 
$Form.Controls.Add($Tb_Gewicht) 

#Anzeigetext Grösse
$Groesse = New-Object System.Windows.Forms.Label
$Groesse.Location = New-Object System.Drawing.Size($fAbstand, (5*$fAbstand)) 
$Groesse.Size = New-Object System.Drawing.Size(($fBreite - 2*$fAbstand),$fHoehe)  
$Groesse.Text = "Grösse in m:"
$Form.Controls.Add($Groesse) 

#Textbox Groesseneingabe
$Tb_Groesse = New-Object System.Windows.Forms.TextBox 
$Tb_Groesse.Location = New-Object System.Drawing.Size($fAbstand, (6*$fAbstand)) 
$Tb_Groesse.Size = New-Object System.Drawing.Size(100,$fHoehe) 
$Form.Controls.Add($Tb_Groesse) 

#Berechnen-Button 7.Zeile Links
$BButton = New-Object System.Windows.Forms.Button
$BButton.Location = New-Object System.Drawing.Size($fAbstand,(7*$fAbstand))
$BButton.Size = New-Object System.Drawing.Size(100,$fHoehe)
$BButton.Text = "Berechnen..."
$BButton.Add_Click({ Berechnen }) #Aufruf der Berechnenfunktion
$Form.Controls.Add($BButton)

#Cancel-Button 7.Zeile rechts
$CancelButton = New-Object System.Windows.Forms.Button
$CancelButton.Location = New-Object System.Drawing.Size(($fAbstand + 120),(7*$fAbstand))
$CancelButton.Size = New-Object System.Drawing.Size(80,$fHoehe)
$CancelButton.Text = "Abbruch"
$CancelButton.Add_Click({$result=$Tb_Name.Text; $Form.Close()})
$Form.Controls.Add($CancelButton)

#Anzeigetext BMI
$BMI = New-Object System.Windows.Forms.Label
$BMI.Location = New-Object System.Drawing.Size($fAbstand, (8*$fAbstand)) 
$BMI.Size = New-Object System.Drawing.Size(100,$fHoehe)  
$BMI.Text = "BMI: ?"
$Form.Controls.Add($BMI) 

#Anzeigetext Klassierung
$Klass = New-Object System.Windows.Forms.Label
$Klass.Location = New-Object System.Drawing.Size($fAbstand, (9*$fAbstand)) 
$Klass.Size = New-Object System.Drawing.Size(($fBreite - 2*$fAbstand),$fHoehe)  
$Klass.Text = ">"
$Form.Controls.Add($Klass) 


#Initialisirung und starten des Formulars
$Form.Add_Shown({$Form.Activate()})
#Falls Abbruch mit [X]...
$answer="Cancel"
$Form.ShowDialog() | Out-Null
