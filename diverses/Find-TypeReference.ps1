<#
.SYNOPSIS
    This script will launch the MSDN or Bing search page for the type passed 
    in as a parameter.
    
.DESCRIPTION
    This script launches your default HTTP URL handler, opening the MSDN or 
    Bing search page with the specified Type as the query.  The default 
    behavior is to search MSDN since the majority of useful type information 
    will exist on the Microsoft website.  Bing is included to allow for type 
    searches that aren't part of the .Net Framework or some other Microsoft
    product.

    This script uses cmdletbinding as well as shouldprocess with a 
    confirmimpact set to high to prevent the user from launching too many 
    search engine windows at once.
    
    This script supports -WhatIf -Confirm and -Verbose

    The MIT License
    -----------------------------------------------------------------------
    Copyright (c) 2012 Tom Nolan <tom at tinyint dot com>

    Permission is hereby granted, free of charge, to any person obtaining a 
    copy of this software and associated documentation files (the "Software"), 
    to deal in the Software without restriction, including without limitation 
    the rights to use, copy, modify, merge, publish, distribute, sublicense, 
    and/or sell copies of the Software, and to permit persons to whom the 
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included 
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN 
    NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
    DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
    USE OR OTHER DEALINGS IN THE SOFTWARE.
    
.NOTES
    Command Name : Find-TypeReference.ps1
    Command Type : Script
    Author       : Tom Nolan - <tom at tinyint dot com>
    Updated      : 2012/02/16
    
.EXAMPLE
    .\Find-TypeReference.ps1 -Type [type]"String"
    This will open MSDN in your default browser and search for "System.String"
    
.EXAMPLE
    .\Find-TypeReference.ps1 -Query "System.Diagnostics.Process" -Bing
    This will use the string specified for the query parameter as the query for the search engine being used, in this case, Bing.
    
.EXAMPLE
    Get-ChildItem | Get-Member | .\Find-TypeReference.ps1
    This will get the member information from the output of Get-Process, which is then piped into this script which will open the search page for System.Diagnostics.Process.
    
.PARAMETER Type
Specify a Type that you want to look up.

.PARAMETER MemberDefinition
This parameter allows you to use Get-Member to retrieve the type name.

.PARAMETER Query
Specify a string that will be used as the search engine query.  While the script is meant to be used for looking up type information, this parameter allows the script to be used for general search engine queries as well.

.PARAMETER Bing
The default search engine is MSDN, but this switch parameter tells the script to query Bing instead.

.LINK
http://tinyint.com/?p=311
#>

[CmdletBinding(SupportsShouldProcess=$true, ConfirmImpact="High")]
Param(
    [parameter(Mandatory=$true,ParameterSetName="Type",ValueFromPipeline=$true)]
    [type]$Type,
    [parameter(Mandatory=$true,ParameterSetName="MemberDefinition",ValueFromPipeline=$true)]
    [Microsoft.PowerShell.Commands.MemberDefinition]$MemberDefinition,
    [parameter(Mandatory=$true,ParameterSetName="Query",ValueFromPipeline=$true)]
    [string]$Query,
    [switch]$Bing
)
Begin { 
    if ($Bing)
        { $search = "http://www.bing.com/search?q={0}" }
    else
        { $search = "http://social.msdn.microsoft.com/search/de?query={0}" }
    
    Write-Verbose "Search engine set to $($search -f 'query')"
    
    $typeList = New-Object System.Collections.ArrayList
}
Process
{
    switch ($PsCmdlet.ParameterSetName)
    {
        "Type" { $searchType = $Type.FullName }
        "MemberDefinition" { $searchType = ($MemberDefinition.TypeName -replace '^(Selected\.|CSV:|Deserialized\.)+(.*)', '$2') }
        "Query" { $searchType = $Query }
    }

    if ($typeList.IndexOf($searchType) -ge 0)
        { Write-Verbose "Skipping already known type: $searchType" }
    else
    {
        Write-Verbose "Found new type: $searchType"
        [Void]($typeList.Add($searchType))
        if ($PsCmdlet.ShouldProcess($searchType, "Launch search engine"))
            { start ( $search -f $searchType ) }
    }
}
End {
    Remove-Variable typeList
}