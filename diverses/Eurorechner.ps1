﻿# Eurorechner
#############
# Autor: Kel
# Version 2.0: 31.10.12 
# Funktion: Umrechnug Franken in Eurobetrag
# Eingabe über Inputbox.ps1 mit Fehlerabfrage und Verarbeitungsloop
# Ausgabe über Show-MsgBox-Funktion, die vorab geladen wird
# Übung 2 für MP-Vorbreitung

# Konstanten-Deklaration:
Set-Variable eurokurs -value 1.21 -option ReadOnly

# Variablen-Deklaration und Initialisation:
[string] $eingabe = ""  	# für Inputbox
[single] $franken = 0		# speichert Frankenbetrag
[single] $euro = 0			# speichert Eurobetrag
[bool]   $weiter = $true	# für Ablaufsteuerung
[String] $scriptpath = Split-Path $MyInvocation.MyCommand.Path
							# aktueller Scriptpfad korrekt initialisieren
# Wechsle zu lokalem Script-Verzeichnis
Set-Location $scriptpath							

#Externe Funktion Show-MsgBox einbinden
. .\Show-MsgBox.ps1

# Hauptschleife: ganze Aufgabe wiederholen
Do {
        # Eingabewiederholung bei Falscheingaben
		Do {
		    #Setzt Flag für neue Eingabe auf "weiter"
		    $weiter = $true
			
			#Eingabe Frankenbetrag als Text
			$eingabe = .\Inputbox.ps1 "Geben Sie ihren Frankenbetrag ein:" "Eurorechner sFr. -> Euro"			
			write $eingabe
			if ($eingabe -eq "Cancel") {
			    write "Programm abgebrochen..."
				Remove-Variable eurokurs -force
				exit
			}
			
			#Umwandlung Text in Zahl (Double) ohne Abbruch bei Fehler (=0)
			$franken = $eingabe -as [double] #Siehe Kapitel 3
			
			#Eingabeüberprüfung (Robustheit)
			if ($franken -eq 0){ 
			   Show-MsgBox "Falsche Eingabe: Bitte Zahl eingeben" "ERROR!" "Exclamation"
			   $weiter = $false
			}
		} Until ($weiter)


		#Berechnen und auf 2 Stellen runden
		$euro = $franken/$eurokurs
		$euro = [Math]::Round($euro,2)
		

		#Ausgabe Resultat
		Show-MsgBox "Umrechnung: `n $franken sFr. sind $euro € ! `n " "Eurorechner: Resultat"
		
		#Abfrage Wiederholung der Berechnung
		$eingabe = Show-MsgBox "Wollen Sie noch eine Berechnung machen?" "Eurorechner..." "Question" "YesNo" 
		$weiter = ($eingabe -eq "Yes") 
		
} While ($weiter) # wiederholen solange "Ja" gedrückt wurde...

write "Bye bye ..."
Remove-Variable eurokurs -force