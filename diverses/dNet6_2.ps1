﻿#Erstellt WebClient zum Einsatz an der TBZ
##########################################

# Script: dNet6_2.ps1
# Autor: KEL
# Datum:9.12.09
# Version: 1.0

#.Net Objekte initialisieren
$web = new-object -typename Net.WebClient
$proxy = New-Object System.Net.WebProxy("http://10.1.200.139:8080")

#Eigenschaften zuweisen:
$proxy.UseDefaultCredentials = $true #Benutzt aktuellen Benutzername & PW
$web.proxy = $proxy #WebClient über diesen Proxy leiten

#Methodenaufruf:
$web.DownloadFile("http://schulemitspass.de/Geheimdienst/Bilder/auto.jpg","H:\auto.jpg")
