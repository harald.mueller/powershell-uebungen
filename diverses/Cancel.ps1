#Erstellt leeres Formular mit Cancel-Button
###########################################

# Script: Cancel.ps1
# Autor: KEL
# Datum:25.10.12
# Version: 1.0
# Version: 1.1 $script:answer wegen neuer Gültigkeitprüfung eingefügt

#.Net Bibliothek Forms laden
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 

#.Net Objekte initialisieren
$form = new-object Windows.Forms.Form
$Form.Size = New-Object System.Drawing.Size(200,200) 
$Form.StartPosition = "CenterScreen"

#Titel
$form.Text = "Beispielfenster"

#Neuer Cancel-Button:
$Cbutton = new-object Windows.Forms.Button
$Cbutton.Text = "Cancel"
$Cbutton.Left = 100 #Position innerhalb des Formulars
$Cbutton.Top = 100
$Cbutton.Width = 50
$Cbutton.Height = 25

#Click-Aktion dem Button beifügen  
$Cbutton.Add_Click( {$script:answer = "Cancel"; $form.close()} )

#Button dem Formular beifügen mit 
$form.Controls.Add($Cbutton)

#Formular starten
$script:answer = "Exit"
$form.ShowDialog() | out-null

#Abfrage der Benutzeeingabe
if ($script:answer -eq "Cancel") {Write-Host Sie haben Cancel gedrückt}
if ($script:answer -eq "Exit") {Write-Host Sie haben [X] gedrückt}
