#Aktueller Pfad zeilenweise ausgeben und einf�rben
##################################################

# Script: ShowPath.ps1
# Autor: KEL
# Datum:19.11.09
# Version: 1.0

#Pfad holen und anpassen
$allpath = $env:path.ToUpper()
$patharray=$allpath.Split(";")

#Konsole s�ubern
CLS
Write-Host "`nPATH:`n=====`n"

#Jede Zeile (Arraywert) testen und entsprechend ausgeben
foreach ($p in $patharray) {
  if ($p -Like "*\WINDOWS*") {
    Write-host -foregroundcolor "red" $p
  } else {
    write-host $p
  }
}