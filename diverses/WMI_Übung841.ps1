﻿$Disks = Get-WMIObject Win32_LogicalDisk -computername "."
"{0,8:n} {1,3:n} {2,7:n} {3,10:n}" -f  "Kennung:","Typ","GB","GB frei" 
ForEach ($d in $Disks) { 
  $typ=$d.drivetype 
  Switch ($typ) { 
    2 {$typ="FDD"}
    3 {$typ="HDD"}
    4 {$typ="Net"}
    5 {$typ="CD "} 
  };
  "{0,8:n} {1,3:n} {2,7:n} {3,10:n}" -f $d.DeviceID, $typ, $($d.Size/1024/1024/1024), $($d.freespace/1024/1024/1024) 
}
