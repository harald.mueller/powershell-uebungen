#Funktion Msgbox
################

# Script: MsgBox.ps1 ["Ausgabetext"] ["Fenstertitel"]
# Funktion zur einfachen Ausgabe von Text! Grösse evtl. anpassen!
# Rückgabewerte: "_OK" bei [OK]
#                "_Cancel" bei [Cancel] oder [X]
# Autor: KEL
# Version 1.0: 25.10.12 für MP-Vorbereitung Übung 1
# Version 1.1: 22.03.15 $script:answer wegen neuer Gültigkeitsprüfung eingefügt

#.Net Bibliothek Forms laden
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 

[Object] $Form = New-Object System.Windows.Forms.Form 
if ($args.Count -gt 1) {
   $Form.Text = $args[1]
} else {  
   $Form.Text = "PS-MsgBox"
}
$Form.Size = New-Object System.Drawing.Size(300,200) 
$Form.StartPosition = "CenterScreen"


#Ok-Button
$OKButton = New-Object System.Windows.Forms.Button
$OKButton.Location = New-Object System.Drawing.Size(75,120)
$OKButton.Size = New-Object System.Drawing.Size(75,23)
$OKButton.Text = "Ok"
$OKButton.Add_Click({$script:answer = "_OK"; $Form.Close()}) #Beendet MsgBox: Rückkehr zum Aufruferscript
$Form.Controls.Add($OKButton)

#Anzeigetext
$Label = New-Object System.Windows.Forms.Label
$Label.Location = New-Object System.Drawing.Size(10,20) 
$Label.Size = New-Object System.Drawing.Size(280,100) 
if ($args.count -gt 0) {
    $Label.Text = $args[0]
} else {  
	$Label.Text = ""
}
$Form.Controls.Add($Label) 

#Tastenaktionen einfügen
$Form.KeyPreview = $True
$Form.Add_KeyDown({if ($_.KeyCode -eq "Enter") 
    {$script:answer = "_OK"; $Form.Close()}}) #Beendet MsgBox: Rückkehr zum Aufruferscript
$Form.Add_KeyDown({if ($_.KeyCode -eq "Escape") 
	{$script:answer = "_Cancel";$Form.Close()}}) 
	
#Initialisirung und starten des Formulars
$Form.Topmost = $True
$Form.Add_Shown({$Form.Activate()})
$script:answer = "_Cancel"
$Form.ShowDialog() |Out-Null

return $script:answer