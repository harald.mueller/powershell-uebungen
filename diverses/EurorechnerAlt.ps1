﻿# Eurorechner
#############
# Autor: Kel
# Version 1.0: 25.10.12 
# Funktion: Umrechnug Franken in Eurobetrag
# Eingabe über Inputbox.ps1 mit Fehlerabfrage und Verarbeitungsloop
# Übung 2 für MP-Vorbreitung

# Konstanten-Deklaration:
Set-Variable eurokurs -value 1.21 -option ReadOnly

# Variablen-Deklaration und Initialisation:
[string] $eingabe = ""  	# für Inputbox
[single] $franken = 0		# speichert Frankenbetrag
[single] $euro = 0			# speichert Eurobetrag
[bool]   $weiter = $true	# für Ablaufsteuerung
[String] $scriptpath = Split-Path $MyInvocation.MyCommand.Path
							# aktueller Scriptpfad korrekt initialisieren

# Wechsle zu lokalem Script-Verzeichnis
Set-Location $scriptpath

# Hauptschleife: ganze Aufgabe wiederholen
Do {
        # Eingabewiederholung bei Falscheingaben
		Do {
		    #Setzt Flag für neue Eingabe auf "weiter"
		    $weiter = $true
			
			#Eingabe Frankenbetrag als Text
			$eingabe = .\Inputbox.ps1 "Geben Sie ihren Frankenbetrag ein:" "Eurorechner sFr. -> Euro"			
			if ($eingabe -eq "_Cancel") {
			    write "Programm abgebrochen..."
				exit
			}
			
			#Umwandlung Text in Zahl (Double) ohne Abbruch bei Fehler (=0)
			$franken = $eingabe -as [double] #Siehe Kapitel 3
			
			#Eingabeüberprüfung (Robustheit)
			if ($franken -eq 0){ 
			   .\Msgbox "Falsche Eingabe: Bitte Zahl eingeben" "ERROR!" | out-null
			   $weiter = $false
			}
		} Until ($weiter)


		#Berechnen und auf 2 Stellen runden
		$euro = $franken/$eurokurs
		$euro = [Math]::Round($euro,2)
		

		#Ausgabe Resultat
		$eingabe = .\MsgBox "Umrechnung: `n $franken sFr. sind $euro € ! `n " "Eurorechner: Resultat"
		if ($eingabe -eq "_Cancel") {
			write "Programm abgebrochen..."
			exit
		}
		
		#Abfrage Wiederholung der Berechnung
		$eingabe = .\YesNoBox "Wollen Sie noch eine Berechnung machen?" "Eurorechner..."
		$weiter = ($eingabe -eq "_Yes") 
		
} While ($weiter) # wiederholen solange "Ja" gedrückt wurde...

write "Bye bye ..."
Remove-Variable eurokurs -force