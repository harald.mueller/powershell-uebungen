﻿# Testet InputBox- und Msgbox-Funktionen (Scripts)
##################################################
# Autor: Kel
# Version 1.1: 31.10.12 

# Aktueller Scriptpfad einlesen und als akt. Pfad setzen
[String] $scriptpath = Split-Path $MyInvocation.MyCommand.Path						
Set-Location $scriptpath

#Einbinden der Show-Msgbox Funktion
. .\Show-MsgBox.ps1

# Startet Inputbox 2x mit versch. Parametern
# und gibt zweite Eingabe in einer MsgBox aus!
# Siehe auch Beschreibung der Funktionen in den Scripts

.\Inputbox.ps1 "Eingabe bitte:" "Demo 1"

$EingabeText = "Nochmals eine Eingabe bitte:"
$TitelText = "Demo 2"
$Eingabe = .\Inputbox $EingabeText $TitelText

Show-MsgBox $Eingabe "Eingabe der Demo 2"
