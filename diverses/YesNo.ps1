#Erstellt Formular mit Yes und No-Button
########################################

# Script: YesNo.ps1
# Autor: KEL
# Datum:17.12.09
# Version: 1.0
# Version: 1.1 $script:answer wegen neuem Gültigkeitsbereich eingeführt!

#.Net Bibliothek Forms laden
[void][reflection.assembly]::LoadWithPartialName("System.Windows.Forms")

#.Net Objekte initialisieren
$form = new-object Windows.Forms.Form
$Form.Size = New-Object System.Drawing.Size(200,200) 
$Form.StartPosition = "CenterScreen"
$script:answer = "Exit"

#Titel
$form.Text = "Frage:"

#Neuer No-Button:
$Cbutton = new-object Windows.Forms.Button
$Cbutton.Text = "No"
$Cbutton.Left = 100 #Position innerhalb des Formulars
$Cbutton.Top = 100
$Cbutton.Width = 50
$Cbutton.Height = 25

#Click-Aktion dem Button beifügen  
$Cbutton.Add_Click( {$script:answer = "No"; $form.close()} )

#Button dem Formular beifügen mit 
$form.Controls.Add($Cbutton)

#Neuer Yes-Button:
$ok = new-object Windows.Forms.Button
$ok.Text = "Yes"
$ok.Left = 50
$ok.Top = 100
$ok.Width = 50
$ok.Height = 25
#Click-Aktion dem Button beifügen  
$ok.Add_Click( {$script:answer = "Yes"; $form.close()} )
#Button dem Formular beifügen mit 
$form.Controls.Add($ok)

#Text (Label) platzieren:
$Label = New-Object System.Windows.Forms.Label
$Label.Left = 50
$Label.Top = 50
$Label.Width = 200
$Label.Height = 20
$Label.Text = "Sind Sie sicher?"
$Form.Controls.Add($Label) 

#Formular starten
$form.ShowDialog() | out-null

#Abfrage der Benutzeeingabe
if ($script:answer -eq "No") {Write-Host Sie haben No gedrückt}
if ($script:answer -eq "Exit") {Write-Host Sie haben [X] gedrückt}
if ($script:answer -eq "Yes") {Write-Host Sie haben Yes gedrückt}