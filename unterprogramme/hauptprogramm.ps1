﻿Write-host "hauptprogramm Beginn"

# dies ist ein Beispiel, was das Hauptprogramm tun könnte
Get-Process WhatsApp  > attachment.txt # Ausgabe in eine neue Datei
Get-StartApps Micro* >> attachment.txt # Ausgabe unten in die gleiche Datei 
Get-Volume           >> attachment.txt # Ausgabe unten in die gleiche Datei
Get-ComputerInfo     >> attachment.txt # Ausgabe unten in die gleiche Datei

# Aufruf des Unterprogramms (Achtung, ein Punkt und Leerschlag am Anfang)
. ".\firmenMail.ps1"


Write-host "hauptprogramm Ende"
