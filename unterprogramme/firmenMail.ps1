﻿# Diese Maileinstellung über GMX hat am 6.9.2020 mehrmals funtioniert. 
# Gesendet ab meinem Wohnort in Uster über WLAN-Provider UPC, 
# ohne Zusatz-Firewall (nur Windows-Defender)


# Bitte löse ein eigenes Konto
# (ich brauch das Mail jedoch privat nicht!)

$SmtpServer   = ""
$SmtpUser     = "" 
$SmtpPassword = "" 


$config = Get-Content ".\firmenMail.config"
# foreach ($c in $config) {
# } 


$SmtpServer = $config[0]
$SmtpUser = $config[1] 
$SmtpPassword = $config[2] 


# schickt mir keine Spams...
$MailtTo = 'harald.mueller@bluewin.ch, harald.mueller@tbz.ch'
$MailFrom = $SmtpUser
$MailSubject = "Mail-Test ueber $SmtpServer" ## hier selber anpassen


# Einlesen des Body
$body = Get-Content ".\firmenMailBody.txt" -Raw -Encoding UTF8


$Credentials = New-Object System.Management.Automation.PSCredential -ArgumentList $SmtpUser, $($SmtpPassword | ConvertTo-SecureString -AsPlainText -Force) 


# Ueber den Parameter -BodyAsHtml  könnte man ein richtig schönes HTML/CSS-formatiertes Mail machen



# Mail mit Attachment (es kommen 3 Dateien, die mit Namen 'attachment' anfangen mit)
Get-ChildItem attachment.* | Send-MailMessage -To "$MailtTo" -From "$MailFrom" -Subject $MailSubject -Body $body -SmtpServer $SmtpServer -UseSsl -Credential $Credentials -Encoding ([System.Text.Encoding]::UTF8)

# ohne Attachment machst Du einfach dies:
# Send-MailMessage -To "$MailtTo" -From "$MailFrom" -Subject $MailSubject -Body $body -SmtpServer $SmtpServer -UseSsl -Credential $Credentials -Encoding ([System.Text.Encoding]::UTF8)
