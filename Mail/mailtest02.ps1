﻿(Get-Credential).password | ConvertFrom-SecureString > mailpasswortGMX.txt
$pw = Get-Content .\mailpasswortGMX.txt | ConvertTo-SecureString
$cred = New-Object System.Management.Automation.PSCredential "MailUser", $pw
$PSEmailServer = "mail.gmx.net"
Send-MailMessage -Credential $cred  -Port 587 -UseSsl -to "harald.mueller@bluewin.ch" -from "haraldmueller@gmx.ch" -Subject "Test" -body "Test für Send-MailMessage"

$body = 
"IHRE RECHNUNG VON AUTO-INTERLEASING AG
VOTRE FACTURE D'AUTO-INTERLEASING SA

Sehr geehrte Damen und Herren
Madame, Monsieur,

Mit dieser Mail erhalten Sie die aktuelle Rechnung von Auto-Interleasing als PDF. Die Papierrechnung erhalten Sie wie gewohnt mit der Post.
Par ce présent courrier, nous vous transmettons la facture d'Auto-Interleasing en version PDF. La facture papier vous parviendra comme d'habitude par courrier postal.

Wir bitten Sie, den offenen Betrag unter Einhaltung der Zahlungsfrist an die auf dem beigefügten Einzahlungsschein vermerkten Kontonummer zu überweisen.
Nous vous prions de verser le montant dû conformément au délai de paiement mentionné sur le bulletin de versement joint.

Einzahlung für: 
Versement pour:

Auto-Interleasing AG
St. Jakob-Strasse 72
4132 Muttenz

IBAN: CH21 0023 3233 1012 5122 0


Freundliche Grüsse,
Avec nos cordiales salutations,

Auto-Interleasing AG
St. Jakob-Strasse 72
4132 Muttenz 
+41 61 319 32 32
info@auto-interleasing.ch
www.auto-interleasing.ch


Für die elektronische Rechnung sind Sie mit obiger E-Mail-Adresse bei uns registriert.
Pour l'envoi des factures par voie électronique, vous êtes inscrits avec l'adresse e-mail mentionnée ci-dessus

Allfällige Änderungen Ihrer E-Mail-Adresse teilen Sie bitte unverzüglich Ihrem Kundenbetreuer mit.
Pour toute modification de votre adresse e-mail, nous vous prions de contacter votre gestionnaire.

Bitte beachten Sie, dass Antworten auf diese E-Mail-Adresse leider nicht beantwortet werden können. 
Im Falle von Rückfragen oder Unklarheiten wenden Sie sich bitte an Ihren zuständigen Kundenbetreuer. 

Vielen Dank!

Veuillez prendre note qu'aucune correspondance ne peut être échangée par le biais de cette adresse e-mail. 
Pour toutes questions ou informations, nous vous prions de contacter votre gestionnaire. Merci beaucoup !
"
$SMTPHost = "smtp.gmail.com"
$SMTPPort = "587"
$Username = "invoice.autointerleasing@gmail.com"
$Password = "invoice77autointerleasing" | ConvertTo-SecureString -AsPlainText -Force    

$subjectFixtext = "RECHNUNG/FACTURE"

$smtp = New-Object System.Net.Mail.SmtpClient($SMTPHost, $SMTPPort);
$smtp.EnableSSL = $true
$smtp.Credentials = New-Object System.Net.NetworkCredential($Username, $Password);

$message = New-Object System.Net.Mail.MailMessage
$message.from = $username
$message.body = $body

# Method to write an email with the logfile as attachement
function sendmail ([String] $toMail, [String] $attachment, [String] $RgNr) {
    # $toMail = "harald.mueller@bluewin.ch"
    # $attachment = "C:\Users\Harald\OneDrive\Technik\mails\data.pdf"

    $message.to.add("$toMail")
    $message.attachments.add("$attachment")
    $message.subject = $subjectFixtext + " Nr. " + $RgNr
   
   echo $toMail
   echo $attachment
    # $smtp.send($message)
}

sendmail ("harald.mueller@bluewin.ch", "C:\Users\Harald\OneDrive\Technik\mails\data.pdf", "1")