﻿# Diese Maileinstellung über GMX hat am 6.9.2020 mehrmals funtioniert. 
# Gesendet ab meinem Wohnort in Uster über WLAN-Provider UPC, 
# ohne Zusatz-Firewall (nur Windows-Defender)

$body = 
'Liebe/r Schüler*in

Hier schreibst Du was Du willst
und eine zweite Linie
und auch noch eine weitere Linie

Mit freundlichen Grüssen
Harald Müller
'
# Ueber den Parameter -BodyAsHtml  könnte man ein richtig schönes HTML/CSS-formatiertes Mail machen

# Bitte löse ein eigenes Konto
# (ich brauch das Mail jedoch privat nicht!)
$SmtpServer = 'mail.gmx.net'
$SmtpUser = 'haraldmueller@gmx.ch' 
$smtpPassword = 'Ausstellungsstrasse70' 

# schickt mir keine Spams...
$MailtTo = 'harald.mueller@bluewin.ch'
$MailFrom = 'haraldmueller@gmx.ch'
$MailSubject = "Mail-Test ueber $SmtpServer"

$Credentials = New-Object System.Management.Automation.PSCredential -ArgumentList $SmtpUser, $($smtpPassword | ConvertTo-SecureString -AsPlainText -Force) 

# Mail mit Attachment (es kommen 3 Dateien, die mit Namen 'attachment' anfangen mit)
Get-ChildItem attachment.* | Send-MailMessage -To "$MailtTo" -From "$MailFrom" -Subject $MailSubject -Body $body -SmtpServer $SmtpServer -UseSsl -Credential $Credentials -Encoding ([System.Text.Encoding]::UTF8)

# ohne Attachment machst Du einfach dies:
# Send-MailMessage -To "$MailtTo" -From "$MailFrom" -Subject $MailSubject -Body $body -SmtpServer $SmtpServer -UseSsl -Credential $Credentials -Encoding ([System.Text.Encoding]::UTF8)
