﻿cls

#########################################################################################

#Attachments für das sende Mail Script

#########################################################################################

#Outfile der MAC-Adresse für den ersten Attachment
Getmac | out-file C:\temp\att1.txt

#Outfile der Bios-Version für den zweiten Attachment
WmiObject Win32_BIOS | select SMBIOSBIOSVersion | out-file C:\temp\att2.txt

#Outfile der Prozessor Informationen für den dritten Attachment
Get-WmiObject win32_processor | ft -AutoSize Name,NumberOfCores,NumberOfLogicalProcessors | out-file C:\temp\att3.txt

#Outfile der Grafikkarten Informationen für den vierten Attachment
Get-WmiObject Win32_VideoController | select name | out-file C:\temp\att4.txt

#Outfile der Arbeitsspeicher Informationen für den fünften Attachment
Get-WMIObject -class Win32_Physicalmemory | select * | out-file C:\temp\att5.txt

#Outfile der Disk Informationen für den sechsten Attachment
Get-Volume | out-file C:\temp\att6.txt

#########################################################################################

#Abruf des sende Mail Script

#########################################################################################

. "C:\temp\Modul122\Planzer_Mail.ps1"

#########################################################################################

# Mail senden über das PowerShell Send-MailMassage

#########################################################################################

Send-MailMessage -From $mailTo -To $mailTo -Subject $subj -Body $body -Attachments $att -SmtpServer 'smtp1.planzer.ch'

#########################################################################################

